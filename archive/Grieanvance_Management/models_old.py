from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Master_Gender(models.Model):
    gender_ID = models.AutoField(primary_key=True)
    Gender = models.CharField(max_length=10)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.BooleanField(default=True)
    createdDate = models.DateTimeField(auto_now_add=True)
    updatedDate = models.DateTimeField(auto_now_add=True)


class Master_Zone(models.Model):
    zone_id = models.AutoField(primary_key=True)
    zone_name = models.CharField(max_length=50)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.BooleanField(default=True)
    createdDate = models.DateTimeField(auto_now_add=True)
    updatedDate = models.DateTimeField(auto_now_add=True)


class Master_State(models.Model):
    state_id = models.AutoField(primary_key=True)
    zone_id = models.ForeignKey(Master_Zone, on_delete=models.CASCADE)
    state_name = models.CharField(max_length=50)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.BooleanField(default=True)
    createdDate = models.DateTimeField(auto_now_add=True)
    updatedDate = models.DateTimeField(auto_now_add=True)


class Master_Region(models.Model):
    region_id = models.AutoField(primary_key=True)
    zone_id = models.ForeignKey(Master_Zone, on_delete=models.CASCADE)
    state_id = models.ForeignKey(Master_State, on_delete=models.CASCADE)
    region_name = models.CharField(max_length=50)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.BooleanField(default=True)
    createdDate = models.DateTimeField(auto_now_add=True)
    updatedDate = models.DateTimeField(auto_now_add=True)


class Master_Branch(models.Model):
    branch_id = models.AutoField(primary_key=True)
    zone_id = models.ForeignKey(Master_Zone, on_delete=models.CASCADE)
    state_id = models.ForeignKey(Master_State, on_delete=models.CASCADE)
    region_id = models.ForeignKey(Master_Region, on_delete=models.CASCADE)
    branch_name = models.CharField(max_length=50)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.BooleanField(default=True)
    createdDate = models.DateTimeField(auto_now_add=True)
    updatedDate = models.DateTimeField(auto_now_add=True)


class Master_Department(models.Model):
    department_id = models.AutoField(primary_key=True)
    department = models.CharField(max_length=50)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.BooleanField(default=True)
    createdDate = models.DateTimeField(auto_now_add=True)
    updatedDate = models.DateTimeField(auto_now_add=True)


class Master_Designation(models.Model):
    desg_id = models.AutoField(primary_key=True)
    department = models.ForeignKey(Master_Department, on_delete=models.CASCADE)
    desg = models.CharField(max_length=50)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.BooleanField(default=True)
    createdDate = models.DateTimeField(auto_now_add=True)
    updatedDate = models.DateTimeField(auto_now_add=True)


class Master_Employee(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    employee_id = models.CharField(max_length=50)
    phone_number = models.BigIntegerField()
    gender = models.ForeignKey(Master_Gender, on_delete=models.CASCADE)
    dob = models.DateField()
    marital_status = models.CharField(max_length=50)
    zone_id = models.ForeignKey(Master_Zone, on_delete=models.CASCADE)
    state_id = models.ForeignKey(Master_State, on_delete=models.CASCADE)
    region_id = models.ForeignKey(Master_Region, on_delete=models.CASCADE)
    branch_id = models.ForeignKey(Master_Branch, on_delete=models.CASCADE)
    department = models.ForeignKey(Master_Department, on_delete=models.CASCADE)
    designation = models.ForeignKey(Master_Designation, on_delete=models.CASCADE)
    Address = models.TextField()
    status = models.BooleanField(default=True)
    createdDate = models.DateTimeField(auto_now_add=True)
    updatedDate = models.DateTimeField(auto_now_add=True)


class Customer_details(models.Model):
    customer_ID = models.ForeignKey(User, on_delete=models.CASCADE)
    customer_Name = models.CharField(max_length=230)
    DOB = models.DateField()
    Marital_Status = models.CharField(max_length=50)
    Income = models.IntegerField()
    Mobile_Number = models.IntegerField()
    Email_id = models.EmailField()
    Address = models.TextField()
    zone_id = models.ForeignKey(Master_Zone, on_delete=models.CASCADE)
    state_id = models.ForeignKey(Master_State, on_delete=models.CASCADE)
    region_id = models.ForeignKey(Master_Region, on_delete=models.CASCADE)
    branch_id = models.ForeignKey(Master_Branch, on_delete=models.CASCADE)
    pin_code = models.IntegerField()
    Kyc = [('PAN', 'PAN'), ('Aadhar', 'Aadhar'), ('Voter ID', 'Voter ID'),
           ('Passport', 'Passport')]
    kyc_type = models.CharField(max_length=100, choices=Kyc)
    kyc_number = models.CharField(max_length=20)


class Vehicle_details(models.Model):
    Customer_details = models.ForeignKey(Customer_details, on_delete=models.CASCADE)
    vehicle_no = models.CharField(max_length=230)
    Model_No = models.IntegerField()
    Price = models.IntegerField()
    Variant = models.CharField(max_length=230)
    Down_payment = models.IntegerField()
    Rate_of_Interest = models.IntegerField()
    Requested_EMI = models.IntegerField()
    Tenure = models.TextField()
    Comment = models.TextField()


class Ticket_Create(models.Model):
    Case_ID = models.AutoField(primary_key=True)
    Customer_details = models.ForeignKey(Customer_details, on_delete=models.CASCADE, null=True)
    Category = models.IntegerField()
    Description = models.TextField()
    createdDate = models.DateTimeField(auto_now_add=True)


class Ticket_Info(models.Model):
    customer_ID = models.ForeignKey(Ticket_Create, on_delete=models.CASCADE, null=True)
    tick_status = [('Inprogress', 'Inprogress'), ('Pending', 'Pending'), ('Completed', 'Completed'),
                   ('Deactivate', 'Deactivate')]
    ticket_status = models.CharField(max_length=100, choices=tick_status)
    Resolver_Description = models.TextField()
    RCA = models.TextField()
    reassign_to = models.ForeignKey(User, on_delete=models.CASCADE)
    reassign_dept = models.ForeignKey(Master_Department, on_delete=models.CASCADE)
    updatedDate = models.DateTimeField(auto_now_add=True)
