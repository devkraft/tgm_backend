from django.contrib import admin
from . models_old import Master_Gender,Master_Zone,Master_State,Master_Region,Master_Branch,Master_Department,\
    Master_Designation,Master_Employee,Customer_details,Vehicle_details,Ticket_Create,Ticket_Info

# Register your models here.
admin.site.register(Master_Gender)
admin.site.register(Master_Zone)
admin.site.register(Master_State)
admin.site.register(Master_Region)
admin.site.register(Master_Branch)
admin.site.register(Master_Department)
admin.site.register(Master_Designation)
admin.site.register(Master_Employee)
admin.site.register(Customer_details)
admin.site.register(Vehicle_details)
admin.site.register(Ticket_Create)
admin.site.register(Ticket_Info)


