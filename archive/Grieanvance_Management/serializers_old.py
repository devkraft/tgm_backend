from rest_framework import serializers
from .models_old import (Master_Gender, Master_Zone, Master_State, Master_Region, Master_Branch, Master_Department, Master_Designation,
                         Master_Employee, Customer_details, Vehicle_details, Ticket_Create, Ticket_Info)

class Master_GenderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Master_Gender
        fields = "__all__"

class Master_ZoneSerializer(serializers.ModelSerializer):
    class Meta:
        model =Master_Zone
        fields = ('zone_id', 'zone_name', 'created_by', 'status', 'createdDate', 'updatedDate')

class Master_StateSerializer(serializers.ModelSerializer):
    class Meta:
        model =Master_State
        fields = ('state_id', 'zone_id','state_name','created_by', 'status', 'createdDate', 'updatedDate')

class Master_RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Master_Region
        fields = ('region_id', 'zone_id','state_id','state_name','region_name','created_by', 'status', 'createdDate', 'updatedDate')

class Master_BranchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Master_Branch
        fields = ('branch_id','zone_id','state_id','state_name','region_id','region_name','branch_name''created_by', 'status', 'createdDate', 'updatedDate')

class Master_DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model =Master_Department
        fields = ('department_id ', 'department','state_name','created_by', 'status', 'createdDate', 'updatedDate')

class Master_DesignationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Master_Designation
        fields = ('desg_id', 'department','desg','state_name','created_by', 'status', 'createdDate', 'updatedDate')


class Master_EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Master_Employee
        fields = ('user_id ', 'employee_id ','phone_number','gender','dob','marital_status','zone_id','state_id','region_id',
                  'branch_id','department','designation','created_by', 'Address','status', 'createdDate', 'updatedDate')




class Customer_detailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer_details
        fields = ('customer_ID','customer_Name','DOB','Marital_Status','Income','Mobile_Number','Email_id','Address','town','Dist','state',
                  'pin_code','kyc_type','kyc_number')

class Get_CustomerIDSerializer(serializers.Serializer):
    customer_ID = serializers.IntegerField()

class Vehicle_detailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle_details
        fields = ('Customer_details','vehicle_no','Model_No','Price','Variant','Down_payment','Rate_of_Interest','Requested_EMI','Tenure','Comment')

# class Get_VechileDetailsSerializer(serializers.Serializer):
#     customer_ID = serializers.IntegerField()

class TicketCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ticket_Create
        fields = ('Customer_details','Category','Description')

class TicketInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ticket_Info
        fields = ('customer_ID','ticket_status','Resolver_Description','RCA','Resoler_ID','updatedDate')