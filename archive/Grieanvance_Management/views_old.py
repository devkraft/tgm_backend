from django.db.models import Q
from django.shortcuts import render
from . models_old import (Master_Gender, Master_Zone, Master_State, Master_Region, Master_Branch,
                          Master_Department, Master_Designation, Master_Employee, Customer_details,
                          Ticket_Info, Ticket_Create, Vehicle_details)
from .serializers_old import (Master_GenderSerializer, Master_ZoneSerializer, Master_StateSerializer,
                              Customer_detailsSerializer, Vehicle_detailsSerializer, Get_CustomerIDSerializer,
                              TicketCreateSerializer, TicketInfoSerializer, Master_RegionSerializer,
                              Master_BranchSerializer, Master_DepartmentSerializer, Master_DesignationSerializer,
                              Master_EmployeeSerializer)
from rest_framework.generics import ListCreateAPIView
from rest_framework import status, generics
from django.http import HttpResponse,JsonResponse
from rest_framework.response import Response
from rest_framework.views import APIView
# import pymysql
import pandas as pd
import json


# class Master_Gender(APIView):
#     def post(self, request, format=None):
#         serializer = Master_GenderSerializer(data=request.data)
#
#         if not serializer.is_valid():
#             return Response(
#                 {
#                     "success": False,
#                     "errors": serializer.errors
#                 },
#                 status=status.HTTP_422_UNPROCESSABLE_ENTITY)
#
#         gender_ID = serializer.validated_data['gender_ID']
#         Gender  = serializer.validated_data['Gender']
#         created_by  = serializer.validated_data['created_by']
#         generalstatus = serializer.validated_data['generalstatus']
#         createdDate  = serializer.validated_data['createdDate']
#         updatedDate = serializer.validated_data['updatedDate']
#
#
#         # return Response({"success": True,"result": "Master_Gender Inserted Successfully..!"},status=status.HTTP_200_OK)
#
#         serializer.create(validated_data=serializer.validated_data)
#
#         return Response(
#             {
#                 "success": True,
#                 "result": "Master_Gender Information Inserted Successfully..!"
#             },
#             status=status.HTTP_200_OK
#         )
#
#     def get(self, request, format=None):
#         data = Master_Gender.objects.all()
#         data = Master_GenderSerializer(data, many=True)
#         return Response(data.data,status=status.HTTP_200_OK)
#
#
# class Master_Zone(APIView):
#     def post(self, request, format=None):
#         serializer = Master_ZoneSerializer(data=request.data)
#
#         if not serializer.is_valid():
#             return Response(
#                 {
#                     "success": False,
#                     "errors": serializer.errors
#                 },
#                 status=status.HTTP_422_UNPROCESSABLE_ENTITY)
#
#         zone_id = serializer.validated_data['gender_ID']
#         zone_name = serializer.validated_data['Gender']
#         created_by = serializer.validated_data['created_by']
#         generalstatus = serializer.validated_data['generalstatus']
#         createdDate = serializer.validated_data['createdDate']
#         updatedDate = serializer.validated_data['updatedDate']
#
#         # return Response({"success": True,"result": "Master_Zone Inserted Successfully..!"},status=status.HTTP_200_OK)
#
#         serializer.create(validated_data=serializer.validated_data)
#
#         return Response(
#             {
#                 "success": True,
#                 "result": "Master_Zone Information Inserted Successfully..!"
#             },
#             status=status.HTTP_200_OK
#         )
#
#     def get(self, request, format=None):
#         data = Master_Zone.objects.all()
#         data = Master_ZoneSerializer(data, many=True)
#         return Response(data.data,status=status.HTTP_200_OK)
#
# class Master_State(APIView):
#     def post(self, request, format=None):
#         serializer = Master_StateSerializer(data=request.data)
#
#         if not serializer.is_valid():
#             return Response(
#                 {
#                     "success": False,
#                     "errors": serializer.errors
#                 },
#                 status=status.HTTP_422_UNPROCESSABLE_ENTITY)
#
#         state_id = serializer.validated_data['state_id']
#         zone_id = serializer.validated_data['zone_id']
#         state_name = serializer.validated_data['state_name']
#         created_by = serializer.validated_data['created_by']
#         generalstatus = serializer.validated_data['generalstatus']
#         createdDate = serializer.validated_data['createdDate']
#         updatedDate = serializer.validated_data['updatedDate']
#
#         # return Response({"success": True,"result": "Master_State Inserted Successfully..!"},status=status.HTTP_200_OK)
#
#         serializer.create(validated_data=serializer.validated_data)
#
#         return Response(
#             {
#                 "success": True,
#                 "result": "Master_State Information Inserted Successfully..!"
#             },
#             status=status.HTTP_200_OK
#         )
#
#     def get(self, request, format=None):
#         data = Master_State.objects.all()
#         data = Master_StateSerializer(data, many=True)
#         return Response(data.data,status=status.HTTP_200_OK)
#
# class Master_Region(APIView):
#     def post(self, request, format=None):
#         serializer = Master_RegionSerializer(data=request.data)
#
#         if not serializer.is_valid():
#             return Response(
#                 {
#                     "success": False,
#                     "errors": serializer.errors
#                 },
#                 status=status.HTTP_422_UNPROCESSABLE_ENTITY)
#
#         region_id = serializer.validated_data['region_id']
#         zone_id = serializer.validated_data['zone_id']
#         state_id = serializer.validated_data['state_id']
#         region_name = serializer.validated_data['region_name']
#         created_by = serializer.validated_data['created_by']
#         generalstatus = serializer.validated_data['generalstatus']
#         createdDate = serializer.validated_data['createdDate']
#         updatedDate = serializer.validated_data['updatedDate']
#
#         # return Response({"success": True,"result": "Master_Region Inserted Successfully..!"},status=status.HTTP_200_OK)
#
#         serializer.create(validated_data=serializer.validated_data)
#
#         return Response(
#             {
#                 "success": True,
#                 "result": "Master_Region Information Inserted Successfully..!"
#             },
#             status=status.HTTP_200_OK
#         )
#
#     def get(self, request, format=None):
#         data = Master_Region.objects.all()
#         data = Master_RegionSerializer(data, many=True)
#         return Response(data.data,status=status.HTTP_200_OK)
#
#
# class Master_Branch(APIView):
#     def post(self, request, format=None):
#         serializer = Master_BranchSerializer(data=request.data)
#
#         if not serializer.is_valid():
#             return Response(
#                 {
#                     "success": False,
#                     "errors": serializer.errors
#                 },
#                 status=status.HTTP_422_UNPROCESSABLE_ENTITY)
#
#         branch_id = serializer.validated_data['branch_id']
#         zone_id = serializer.validated_data['zone_id']
#         state_id = serializer.validated_data['state_id']
#         region_id = serializer.validated_data['region_id']
#         branch_name = serializer.validated_data['branch_name']
#         created_by = serializer.validated_data['created_by']
#         generalstatus = serializer.validated_data['generalstatus']
#         createdDate = serializer.validated_data['createdDate']
#         updatedDate = serializer.validated_data['updatedDate']
#
#         # return Response({"success": True,"result": "Master_Branch Inserted Successfully..!"},status=status.HTTP_200_OK)
#
#         serializer.create(validated_data=serializer.validated_data)
#
#         return Response(
#             {
#                 "success": True,
#                 "result": "Master_Branch Information Inserted Successfully..!"
#             },
#             status=status.HTTP_200_OK
#         )
#
#     def get(self, request, format=None):
#         data = Master_Branch.objects.all()
#         data = Master_BranchSerializer(data, many=True)
#         return Response(data.data, status=status.HTTP_200_OK)
#
# class Master_Department(APIView):
#     def post(self, request, format=None):
#         serializer = Master_DepartmentSerializer(data=request.data)
#
#         if not serializer.is_valid():
#             return Response(
#                 {
#                     "success": False,
#                     "errors": serializer.errors
#                 },
#                 status=status.HTTP_422_UNPROCESSABLE_ENTITY)
#
#         department_id = serializer.validated_data['department_id']
#         department = serializer.validated_data['department']
#         created_by = serializer.validated_data['created_by']
#         generalstatus = serializer.validated_data['generalstatus']
#         createdDate = serializer.validated_data['createdDate']
#         updatedDate = serializer.validated_data['updatedDate']
#
#         # return Response({"success": True,"result": "Master_Department Inserted Successfully..!"},status=status.HTTP_200_OK)
#
#         serializer.create(validated_data=serializer.validated_data)
#
#         return Response(
#             {
#                 "success": True,
#                 "result": "Master_Department Information Inserted Successfully..!"
#             },
#             status=status.HTTP_200_OK
#         )
#
#     def get(self, request, format=None):
#         data = Master_Department.objects.all()
#         data = Master_DepartmentSerializer(data, many=True)
#         return Response(data.data, status=status.HTTP_200_OK)
#
#
# class Master_Designation(APIView):
#     def post(self, request, format=None):
#         serializer = Master_DesignationSerializer(data=request.data)
#
#         if not serializer.is_valid():
#             return Response(
#                 {
#                     "success": False,
#                     "errors": serializer.errors
#                 },
#                 status=status.HTTP_422_UNPROCESSABLE_ENTITY)
#
#         desg_id = serializer.validated_data['desg_id']
#         department = serializer.validated_data['department']
#         desg = serializer.validated_data['desg']
#         region_id = serializer.validated_data['region_id']
#         created_by = serializer.validated_data['created_by']
#         generalstatus = serializer.validated_data['generalstatus']
#         createdDate = serializer.validated_data['createdDate']
#         updatedDate = serializer.validated_data['updatedDate']
#
#         # return Response({"success": True,"result": "Master_Designation Inserted Successfully..!"},status=status.HTTP_200_OK)
#
#         serializer.create(validated_data=serializer.validated_data)
#
#         return Response(
#             {
#                 "success": True,
#                 "result": "Master_Designation Information Inserted Successfully..!"
#             },
#             status=status.HTTP_200_OK
#         )
#
#     def get(self, request, format=None):
#         data = Master_Designation.objects.all()
#         data = Master_DesignationSerializer(data, many=True)
#         return Response(data.data, status=status.HTTP_200_OK)


class Master_Employee(APIView):
    def post(self, request, format=None):
        serializer = Master_EmployeeSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(
                {
                    "success": False,
                    "errors": serializer.errors
                },
                status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        user_id = serializer.validated_data['user_id']
        employee_id = serializer.validated_data['employee_id']
        phone_number = serializer.validated_data['phone_number']
        gender = serializer.validated_data['gender']
        dob = serializer.validated_data['dob']
        marital_status = serializer.validated_data['marital_status']
        zone_id = serializer.validated_data['zone_id']
        state_id = serializer.validated_data['state_id']
        branch_id = serializer.validated_data['branch_id']
        region_id = serializer.validated_data['region_id']
        department = serializer.validated_data['department']
        designation = serializer.validated_data['designation']
        Address = serializer.validated_data['Address']
        generalstatus = serializer.validated_data['generalstatus']
        createdDate = serializer.validated_data['createdDate']
        updatedDate = serializer.validated_data['updatedDate']

        # return Response({"success": True,"result": "Master_Employee Inserted Successfully..!"},status=status.HTTP_200_OK)

        serializer.create(validated_data=serializer.validated_data)

        return Response(
            {
                "success": True,
                "result": "Master_Employee Information Inserted Successfully..!"
            },
            status=status.HTTP_200_OK
        )

    def get(self, request, format=None):
        data = Master_Employee.objects.all()
        data = Master_EmployeeSerializer(data, many=True)
        return Response(data.data, status=status.HTTP_200_OK)









class Customer_detailsApiView(APIView):
    def post(self, request, format=None):
        serializer = Customer_detailsSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(
                {
                    "success": False,
                    "errors": serializer.errors
                },
                status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        customer_ID = serializer.validated_data['customer_ID']
        customer_Name = serializer.validated_data['customer_Name']
        DOB = serializer.validated_data['DOB']
        Marital_Status = serializer.validated_data['Marital_Status']
        Income = serializer.validated_data['Income']
        Mobile_Number = serializer.validated_data['Mobile_Number']
        Email_id = serializer.validated_data['Address']
        Address = serializer.validated_data['customer_Name']
        town = serializer.validated_data['town']
        Dist = serializer.validated_data['Dist']
        state = serializer.validated_data['state']
        pin_code = serializer.validated_data['customer_Name']
        kyc_type = serializer.validated_data['kyc_type']
        kyc_number = serializer.validated_data['kyc_number']
        serializer.create(validated_data=serializer.validated_data)
        return Response({"success": True,"result": "Customer Details Saved Successfully..!"},status=status.HTTP_200_OK)

    serializer_class = Get_CustomerIDSerializer
    def get(self, request):
        query_params = self.request.query_params
        customer_ID = query_params.get('customer_ID')
        try:
            # con = pymysql.connect(user='root', host='localhost', database='tvs_griv', password="1234")
            sql_data = pd.read_sql_query(f"select * from Grieanvance_Management_customer_details where customer_ID_id = {customer_ID};", con)
            json_data = sql_data.to_json(orient="records")
            result = json.loads(json_data)
            return Response({"success": True,'result': result},status=status.HTTP_200_OK)
        except:
            raise
class Vehicle_detailsApiView(APIView):
        def post(self, request, format=None):
            serializer = Vehicle_detailsSerializer(data=request.data)

            if not serializer.is_valid():
                return Response(
                    {
                        "success": False,
                        "errors": serializer.errors
                    },
                    status=status.HTTP_422_UNPROCESSABLE_ENTITY)

            Customer_details = serializer.validated_data['Customer_details']
            vehicle_no = serializer.validated_data['vehicle_no']
            Model_No = serializer.validated_data['Model_No']
            Price = serializer.validated_data['Price']
            Variant = serializer.validated_data['Variant']
            Down_payment = serializer.validated_data['Down_payment']
            Rate_of_Interest = serializer.validated_data['Rate_of_Interest']
            Requested_EMI = serializer.validated_data['Requested_EMI']
            Tenure = serializer.validated_data['Tenure']
            Comment = serializer.validated_data['Comment']
            serializer.create(validated_data=serializer.validated_data)

            return Response(
                {
                    "success": True,
                    "result": "Vechicles Details Inserted Successfully..!"
                },
                status=status.HTTP_200_OK
            )

        serializer_class = Get_CustomerIDSerializer

        def get(self, request):
            query_params = self.request.query_params
            customer_ID = query_params.get('customer_ID')
            try:
                # con = pymysql.connect(user='root', host='localhost', database='tvs_griv', password="1234")
                sql_data = pd.read_sql_query(
                    f"select * from Grieanvance_Management_vehicle_details where Customer_details_id = {customer_ID};", con)
                json_data = sql_data.to_json(orient="records")
                result = json.loads(json_data)
                return Response({"success": True, 'result': result}, status=status.HTTP_200_OK)
            except:
                raise


class TicketCreateApiView(APIView):
    def post(self, request, format=None):
        serializer = TicketCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(
                {
                    "success": False,
                    "errors": serializer.errors
                },
                status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        Customer_details = serializer.validated_data['Customer_details']
        Category = serializer.validated_data['Category']
        Description = serializer.validated_data['Description']
        serializer.create(validated_data=serializer.validated_data)
        return Response(
            {
                "success": True,
                "result": "Ticket Has been Created"
            },
            status=status.HTTP_200_OK
        )

    serializer_class = Get_CustomerIDSerializer

    def get(self, request):
        query_params = self.request.query_params
        customer_ID = query_params.get('customer_ID')
        try:
            # con = pymysql.connect(user='root', host='localhost', database='tvs_griv', password="1234")
            sql_data = pd.read_sql_query(
                f"select * from Grieanvance_Management_ticket_create where Customer_details_id = {customer_ID};", con)
            json_data = sql_data.to_json(orient="records")
            result = json.loads(json_data)
            return Response({"success": True, 'result': result}, status=status.HTTP_200_OK)
        except:
            raise

class TicketInfoApiView(APIView):
    def post(self, request, format=None):
        serializer = TicketInfoSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(
                {
                    "success": False,
                    "errors": serializer.errors
                },
                status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        customer_ID = serializer.validated_data['customer_ID']
        ticket_status = serializer.validated_data['ticket_status']
        Resolver_Description = serializer.validated_data['Resolver_Description']
        RCA = serializer.validated_data['RCA']
        Resoler_ID = serializer.validated_data['Resoler_ID']
        serializer.create(validated_data=serializer.validated_data)
        return Response(
            {
                "success": True,
                "result": "Ticket Details has been updated..!"
            },
            status=status.HTTP_200_OK
        )

    serializer_class = Get_CustomerIDSerializer

    def get(self, request):
        query_params = self.request.query_params
        customer_ID = query_params.get('customer_ID')
        try:
            # con = pymysql.connect(user='root', host='localhost', database='tvs_griv', password="1234")
            sql_data = pd.read_sql_query(
                f"select * from Grieanvance_Management_ticket_info where customer_ID_id = {customer_ID};", con)
            json_data = sql_data.to_json(orient="records")
            result = json.loads(json_data)
            return Response({"success": True, 'result': result}, status=status.HTTP_200_OK)
        except:
            raise

class MasterGenderListCrtAPIView(ListCreateAPIView):
    queryset  = Master_Gender.objects.all()
    serializer_class = Master_GenderSerializer

    def get_queryset(self, *args, **kwargs):
        queryset_list = Master_Gender.objects.all()
        query = self.request.GET.get("gender_ID")
        if query:
            queryset_list = queryset_list.filter(
                Q(gender_ID__icontains=query)
            ).distinct()
        return queryset_list
