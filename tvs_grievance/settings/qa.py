# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False


ALLOWED_HOSTS = ["qa-gravience.devkraft.in"]
# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'tvs_management',
        'USER': 'devkraft',
        'PASSWORD': 'devkraft1234',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}