from .base import *
import os
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent.parent.parent


key = 'DJANGOENV'
# you need to set "DJANGOENV = 'prod'" as an environment variable
# in your OS (on which your website is hosted)
if os.getenv(key) == 'prod' and str(BASE_DIR) == "/home/ubuntu/tgm_dev":
   from .dev import *
elif os.getenv(key) == 'prod' and str(BASE_DIR) == "/home/ubuntu/tgm":
   from .qa import *
else:
   from .local import *