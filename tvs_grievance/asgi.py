"""
ASGI config for tvs_grievance project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""

import os
from pathlib import Path
from django.core.wsgi import get_wsgi_application

BASE_DIR = Path(__file__).resolve().parent.parent.parent


key = 'DJANGOENV'
# you need to set "DJANGOENV = 'prod'" as an environment variable
# in your OS (on which your website is hosted)
if os.getenv(key) == 'prod' and str(BASE_DIR) == "/home/ubuntu/tgm_dev":
   os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tvs_grievance.settings.dev')
elif os.getenv(key) == 'prod' and str(BASE_DIR) == "/home/ubuntu/tgm":
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tvs_grievance.settings.qa')
else:
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tvs_grievance.settings.local')

application = get_wsgi_application()
