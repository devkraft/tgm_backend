"""Tvs_Grieavance URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from archive.Grieanvance_Management import views_old

urlpatterns = [
    path('admin/', admin.site.urls),
    path('customer/', views_old.Customer_detailsApiView.as_view()),
    path('vechile/', views_old.Vehicle_detailsApiView.as_view()),
    path('ticket-create/', views_old.TicketCreateApiView.as_view()),
    path('ticket-info/', views_old.TicketInfoApiView.as_view()),
    path('master-gender/', views_old.MasterGenderListCrtAPIView.as_view())
]
