"""tvs_grievance URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from rest_framework import routers
from django.conf.urls.static import static
from django.urls import path, include, resolve
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView, TokenVerifyView,
)
from rest_framework.documentation import include_docs_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    #path('docs/', include_docs_urls(title='TVS Grievance Management APIs', public=False)),
    path('api/', include('grievance_management.urls')),
    path('api/notifications/', include('notifications.urls')),
    path('api/get_token/', include('jwt_auth.urls'))
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


