from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import ReadOnlyPasswordHashField
User = get_user_model()


class UserAdminCreationForm(forms.ModelForm):
    """
    A form for creating new users. Includes all the required
    fields, plus a repeated password.
    """
    USER_TYPE_CHOICES =(
        ('dealer', 'Dealer'),
        ('cs_area', 'CS Area'),
        ('cs_ho', 'CS HomeOffice'),
        ('cs_rf', 'CS Retail Finance'))
    user_type = forms.CharField(label='Choose User Type',widget=forms.Select(choices=USER_TYPE_CHOICES))
    first_name = forms.CharField(label="First Name", required=False)
    last_name = forms.CharField(label="Last Name", required=False)
    username = forms.CharField(label="Username", required=False)
    password = forms.CharField(widget=forms.PasswordInput)
    password_2 = forms.CharField(label='Confirm Password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = '__all__'

    def clean(self):
        '''
        Verify both passwords match and other required details are filed
        '''
        cleaned_data = super().clean()
        user_type = cleaned_data.get("user_type")
        password = cleaned_data.get("password")
        password_2 = cleaned_data.get("password_2")
        if user_type is None:
            user_type = "dealer"
        if password is not None and password != password_2:
            self.add_error("password_2", "Your passwords must match")
        return cleaned_data

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.username = self.cleaned_data.get("email").split("@")[0]
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class UserAdminChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ['email', 'password', 'is_active']

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]