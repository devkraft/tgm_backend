import datetime

class ticket_response_details_json:
    def __init__(self):
        pass
    
    def create_json(self, fields):
        json_data = {
            "process": 
            [
                { "cs_area":
                    {
                        "response_from_cs_area":{
                            "name":"Ticket under review with CS Area team",
                            "response_type": fields[0]["cs_area"]["response_type"],
                            "response": fields[0]["cs_area"]["response"],
                            "rate_of_interest": fields[0]["cs_area"]["int_offered"],
                            "tenure": fields[0]["cs_area"]["tenure"],
                            "downpayment": fields[0]["cs_area"]["down_payment"],
                            "emi": fields[0]["cs_area"]["emi"],
                            "created_time":fields[0]["cs_area"]["created_time"],
                            "action": ""
                        },
                        "dealer_response":{
                            "name":"Response on Resolution/ Offer",
                            "response_type": fields[0]["cs_area"]["dealer_response_type"],
                            "response": fields[0]["cs_area"]["dealer_response"],
                            "created_time":fields[0]["cs_area"]["d_r_created_time"],
                            "action": fields[0]["cs_area"]["action"],
                        }
                    }
                },
                { "cs_ho": 
                    {
                        "response_from_cs_ho":{
                            "name":"Ticket under review with CS Head Office",
                            "response_type": fields[1]["cs_ho"]["response_type"],
                            "response": fields[1]["cs_ho"]["response"],
                            "rate_of_interest": fields[1]["cs_ho"]["int_offered"],
                            "tenure": fields[1]["cs_ho"]["tenure"],
                            "downpayment": fields[1]["cs_ho"]["down_payment"],
                            "emi": fields[1]["cs_ho"]["emi"],
                            "created_time":fields[1]["cs_ho"]["created_time"],
                            "action": ""
                        },
                        "dealer_response":{
                            "name":"Response on Resolution/ Offer",
                            "response_type": fields[1]["cs_ho"]["dealer_response_type"],
                            "response": fields[1]["cs_ho"]["dealer_response"],
                            "created_time":fields[1]["cs_ho"]["d_r_created_time"],
                            "action": fields[1]["cs_ho"]["action"],
                        }
                    }
                },
                { "cs_rf": 
                    {
                        "response_from_retail_finance":{
                            "name":"Ticket under review with Retail Head Office",
                            "response_type": fields[2]["cs_rf"]["response_type"],
                            "response": fields[2]["cs_rf"]["response"],
                            "rate_of_interest": fields[2]["cs_rf"]["int_offered"],
                            "tenure": fields[2]["cs_rf"]["tenure"],
                            "downpayment": fields[2]["cs_rf"]["down_payment"],
                            "emi": fields[2]["cs_rf"]["emi"],
                            "created_time":fields[2]["cs_rf"]["created_time"],
                            "action": ""
                        },
                        "dealer_response":{
                            "name":"Response on Resolution/ Offer",
                            "response_type": fields[2]["cs_rf"]["dealer_response_type"],
                            "response": fields[2]["cs_rf"]["dealer_response"],
                            "created_time":fields[2]["cs_rf"]["d_r_created_time"],
                            "action": fields[2]["cs_rf"]["action"],
                        }
                    }  
                }
            ]
        }
        return json_data
