from django.http import JsonResponse
from django.template import response
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.decorators import action, api_view, authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import Http404
import re
import json
import datetime
from .serializers import *
from django.utils import timezone
from .decorators.auth_decorator import group_required
from grievance_management.serializers import WorkflowSerailizer
from django.db.models import Q, query
from django.views.generic.base import View
from rest_framework.pagination import PageNumberPagination
from grievance_management.models import *
from grievance_management.workflow import workflow_json, workflow_details_json
from grievance_management.ticketResponse import ticket_response_details_json
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import get_user_model
import logging
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent
log_file_path = str(BASE_DIR.parent) + "/logs/cron.log"

logger = logging.getLogger(__name__)

User = get_user_model()
from .cron import set_delayed_cron

@api_view(['POST'])
def login(request):
    try:
        user = None
        if request.method == 'POST':
            password = request.POST['password']
            if "email" in request.POST:
                email = request.POST['email']
                if User.objects.filter(email=email).exists():
                    user = User.objects.filter(email=email).get()
            else:
                response = {'status': 'Bad Request', 'status_code': 400, 'message': 'one or more argument missing in api request', 'error_status': True,
                            'error_code': 4}
                return Response(response)
            
            if user:
                if user.check_password(password):
                    user = authenticate(request, email=email, password=password)
                    if user is not None:
                        auth_login(request._request, user)
                        response = {'status': 'OK', 
                                    'status_code': 200,
                                    'message': 'success',
                                    'user_id': user.id,
                                    'user_type': user.user_type,
                                    'error_status': False,
                                    }
                else:
                    response = {'status': 'Forbidden', 'status_code': 403, 'message': 'wrong password', 'error_status': True, 'error_code': 2}
            else:
                response = {'status': 'Unauthorized', 'status_code': 401, 'message': 'username/email does not exist', 'error_status': True, 'error_code': 1}

            return Response(response)
    except Exception as ex:
        response = {'status': 'Bad Request', 'status_code': 400, 'message': 'Error in' + str(ex), 'error_status': True, 'error_code': 3}
        return Response(response)


def check_special_characters(num):
    string = str(num)
    try:
        if isinstance(float(string), float):
            resp = False

    except Exception as ex:
        resp = True

    return resp

def check_whitespaces(the_String):
    string = str(the_String)
    try:
        if bool(re.match("^[^\s].+[^\s]$", string)) == False:
            resp = string.lstrip().rstrip()

        else:
            resp = string

    except Exception as ex:
        resp = ex
    return resp



@csrf_exempt
@permission_classes([IsAuthenticated])
def create_ticket(request):
    try:
        if request.method == 'GET':
            return JsonResponse({'status': 'Method Not Allowed', 'status_code': 405, 'message': 'Ticket creation API. Please use post request'}, safe=False)
        elif request.method == 'POST':
            data = request.POST
            user_logged_in = data['logged_in_user']
            request_user = user_logged_in

            cust_details = CustomerDetails()
            address = Address()
            user = User.objects.get(email=request_user)

            #Customer Details
            cust_details.created_by = user
            if bool(re.match("^[A-Za-z\s]*$", data['customer_name'])):
                cust_details.customer_Name = check_whitespaces(data['customer_name'])

            else:
                error = ValueError('Customer Name should only contain uppercase or lowercase alphabets')
                raise error

            if datetime.datetime.strptime(data['dob'], '%Y-%m-%d') < datetime.datetime.now():
                dob_object = datetime.datetime.strptime(data['dob'], '%Y-%m-%d')

            else:
                error = ValueError("DOB should be less than today's date")
                raise error

            cust_details.dob = dob_object
            cust_details.mobile_number = data['mobile_number']
            cust_details.email = data['email']
            cust_details.pan_no = data['pan']

            if data['marital_status'] == 'married' or data['marital_status'] == 'unmarried' :
                cust_details.marital_status = data['marital_status']
            else:
                error = ValueError('Please pick the correct marital status.')
                raise error

            if data['income_type'] == 'salaried' or data['income_type'] == 'self_employed' :
                cust_details.income_type = data['income_type']
            else:
                error = ValueError('Please pick the correct Income type.')
                raise error

            if data['income']=='1-3' or data['income']=='3-6' or data['income']=='6-10' or data['income']=='10-15' or data['income']=='>15':
                    cust_details.income = data['income']                       
            else:
                    error = ValueError('Choose the correct Income')
                    raise error


            #Address
            address.address = check_whitespaces(data['address_base'])
            address.city = check_whitespaces(data['city'])
            address.state = data['state']

            if check_special_characters(data['pin']) == False:
                if int(data['pin']) > 0 and len(str(data['pin'])) == 6:
                    address.pin =  int(data['pin'])
                            
                else:
                    error = ValueError('PIN should be a positive 6-digit number')
                    raise error

            else:
                error = ValueError('PIN should be a positive 6-digit number')
                raise error
            

            cust_details.address = address

            cust_details.aadhar = bool(int(data['aadhar']))
            cust_details.passport = bool(int(data['passport']))
            cust_details.driving_license = bool(int(data['driving_license']))
            cust_details.voter_id = bool(int(data['voter_id']))

            cust_details.salary_slip = bool(int(data['salary_slip']))
            cust_details.bank_statement = bool(int(data['bank_statement']))


            address.save()
            cust_details.save()

            vehicle_details = VehicleDetails()
            vehicle_details.customer_details = cust_details
            if data['issue_type'] == 'payment_related':
                vehicle_details.rate_of_interest = float(data['rate_of_interest'])
                vehicle_details.down_payment = float(data['down_payment'])
                vehicle_details.requested_emi = float(data['requested_emi'])
                vehicle_details.tenure = data['tenure']
            
            vehicle_details.comment = check_whitespaces(data['comment'])
            
            if bool(re.match("^[- A-Za-z0-9]*$", data['variant'])):
                vehicle_details.variant = check_whitespaces(data['variant'])

            else:
                error = ValueError('Variant should only contain number and alphabets')
                raise error

            
            if data['issue_type'] == 'payment_related' or data['issue_type'] == 'document_related' or data['issue_type'] == 'other':
                vehicle_details.issue_type = data['issue_type']
            else:
                error = ValueError('Incorrect issue type. Please choose the correct issue type.')
                raise error

            if bool(re.match("^[- A-Za-z0-9]*$", data['model_name'])):
                vehicle_details.model_name = check_whitespaces(data['model_name'])

            else:
                error = ValueError('Model name should only contain number and alphabets')
                raise error

            if check_special_characters(data['price']) == False:
                if float(data['price']) > 0:
                    vehicle_details.price = float(data['price'])
                            
                else:
                    error = ValueError('Price should be a positive number')
                    raise error

            else:
                error = ValueError('Price should be a positive number')
                raise error


            vehicle_details.save()   

            new_ticket = Ticket()
            new_ticket.vehicle_details = vehicle_details
            new_ticket.created_by = cust_details.created_by
            new_ticket.ticket_csat_status='action_pending'
            new_ticket.ticket_dealer_status='in_progress'
            new_ticket.user_grp ='cs_area'
            new_ticket.category = 1
            new_ticket.description = f'Ticket created for vehicle {vehicle_details.model_name}'
            new_ticket.csat_assign_time = timezone.now()
            new_ticket.save()
            
            response = {'status': 'OK', 'status_code': 200, 'message': 'Ticket has been created.', 'ticket_id': new_ticket.id}
            return JsonResponse(response, safe=False)

    except Exception as ex:
        error = f'Error in ticket creation with error: {ex}'
        return JsonResponse({'status': 'Bad Request', 'status_code': 400, 'message': error, 'error_status': True}, safe=False)


@csrf_exempt
@permission_classes([IsAuthenticated])
def customer_details(request):
    try:
        if request.method == 'GET':
            return JsonResponse({'status': 'Method Not Allowed', 'status_code': 405, 'message': 'Customer Details API. Please use post request'}, safe=False)
        elif request.method == 'POST':
            cust_data = request.POST
            user_logged_in = cust_data['logged_in_user']
            request_user = user_logged_in
            cust_details = CustomerDetails()
            address = Address()
            user = User.objects.get(email=request_user)
            
            cust_details.created_by = user
            if bool(re.match("^[A-Za-z\s]*$", cust_data['customer_name'])):
                cust_details.customer_Name = check_whitespaces(cust_data['customer_name'])

            else:
                error = ValueError('Customer Name should only contain uppercase or lowercase alphabets')
                raise error

            if datetime.datetime.strptime(cust_data['dob'], '%Y-%m-%d') < datetime.datetime.now():
                dob_object = datetime.datetime.strptime(cust_data['dob'], '%Y-%m-%d')

            else:
                error = ValueError("DOB should be less than today's date")
                raise error

            cust_details.dob = dob_object
            cust_details.mobile_number = cust_data['mobile_number']
            cust_details.email = cust_data['email']
            cust_details.pan_no = cust_data['pan']

            if cust_data['marital_status'] == 'married' or cust_data['marital_status'] == 'unmarried' :
                cust_details.marital_status = cust_data['marital_status']
            else:
                error = ValueError('Please pick the correct marital status.')
                raise error

            if cust_data['income_type'] == 'salaried' or cust_data['income_type'] == 'self_employed' :
                cust_details.income_type = cust_data['income_type']
            else:
                error = ValueError('Please pick the correct Income type.')
                raise error

            if cust_data['income']=='1-3' or cust_data['income']=='3-6' or cust_data['income']=='6-10' or cust_data['income']=='10-15' or cust_data['income']=='>15':
                    cust_details.income = cust_data['income']                       
            else:
                    error = ValueError('Choose the correct Income')
                    raise error
                

            address.address = check_whitespaces(cust_data['address_base'])
            address.city = check_whitespaces(cust_data['city'])
            address.state = cust_data['state']

            if check_special_characters(cust_data['pin']) == False:
                if int(cust_data['pin']) > 0 and len(str(cust_data['pin'])) == 6:
                    address.pin = int(cust_data['pin'])
                            
                else:
                    error = ValueError('PIN should be a positive 6-digit number')
                    raise error

            else:
                error = ValueError('PIN should be a positive 6-digit number')
                raise error

            cust_details.address = address
            # cust_details.kyc_type = cust_data['kyc_type']
            # cust_details.kyc_number = cust_data['kyc_number']

            address.save()
            cust_details.save()

            response = {'status': 'OK', 'status_code': 200, 'message': 'Customer details has been saved.',
                               'customer_details_id': cust_details.id}

            return JsonResponse(response, safe=False)
    except Exception as ex:
        error = f'Error in customer details with error: {ex}'
        return JsonResponse({'status': 'Bad Request', 'status_code': 400, 'message': error, 'error_status': True}, safe=False)


@csrf_exempt
@permission_classes([IsAuthenticated])
def vehicle_details(request):
    try:
        if request.method == 'GET':
            return JsonResponse({'status': 'Method Not Allowed', 'status_code': 405, 'message': 'Vehicle Details API. Please use post request'}, safe=False)
        elif request.method == 'POST':
            vehicle_data = request.POST
            cust_details_id = vehicle_data['customer_details_id']
            cust_details = CustomerDetails.objects.get(id=cust_details_id)
            vehicle_details = VehicleDetails()
            vehicle_details.customer_details = cust_details
            vehicle_details.comment = check_whitespaces(vehicle_data['comment'])
            if vehicle_data['issue_type'] == 'payment_related':
                vehicle_details.rate_of_interest = float(vehicle_data['rate_of_interest'])
                vehicle_details.down_payment = float(vehicle_data['down_payment'])
                vehicle_details.requested_emi = float(vehicle_data['requested_emi'])
                vehicle_details.tenure = vehicle_data['tenure']

            if vehicle_data['issue_type'] == 'payment_related' or vehicle_data['issue_type'] == 'document_related' or vehicle_data['issue_type'] == 'other':
                vehicle_details.issue_type = vehicle_data['issue_type']
            else:
                error = ValueError('Incorrect issue type. Please choose the correct issue type.')
                raise error
            
            if bool(re.match("^[- A-Za-z0-9]*$", vehicle_data['variant'])):
                vehicle_details.variant = check_whitespaces(vehicle_data['variant'])

            else:
                error = ValueError('Variant should only contain number and alphabets')
                raise error

            if bool(re.match("^[- A-Za-z0-9]*$", vehicle_data['model_name'])):
                vehicle_details.model_name = check_whitespaces(vehicle_data['model_name'])

            else:
                error = ValueError('Model name should only contain number and alphabets')
                raise error

            if check_special_characters(vehicle_data['price']) == False:
                if float(vehicle_data['price']) > 0:
                    vehicle_details.price = float(vehicle_data['price'])
                            
                else:
                    error = ValueError('Price should be a positive number')
                    raise error

            else:
                error = ValueError('Price should be a positive number')
                raise error
            
            vehicle_details.save()
            new_ticket = Ticket()
            new_ticket.vehicle_details = vehicle_details
            # new_ticket.customer_name = vehicle_details
            new_ticket.created_by = cust_details.created_by
            new_ticket.ticket_csat_status='action_pending'
            new_ticket.ticket_dealer_status='in_progress'
            new_ticket.user_grp ='cs_area'
            new_ticket.category = 1
            new_ticket.description = f'Ticket created for vehicle {vehicle_details.model_name}'
            new_ticket.csat_assign_time = timezone.now()
            new_ticket.save()
            response = {'status': 'OK', 'status_code': 200, 'message': 'Vehicle details has been saved.', 'ticket_id': new_ticket.id}
            return JsonResponse(response, safe=False)
    except Exception as ex:
        error = f'Error in vehicle details with error: {ex}'
        return JsonResponse({'status': 'Bad Request', 'status_code': 400, 'message': error, 'error_status': True}, safe=False)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def ticket_summary(request):
    try:
        ticket_id = request.GET['ticket_id']
        ticket_table = Ticket.objects.get(id=ticket_id)
        vehicle_details_table = ticket_table.vehicle_details
        customer_details_table = vehicle_details_table.customer_details
        address_table = customer_details_table.address

        ticket_serializer = TicketSerailizer(ticket_table)
        customer_details_serializer = CustomerDetailsSerializer(customer_details_table)
        vehicle_details_serializer = VehicleDetailsSerializer(vehicle_details_table)
        address_serializer = AddressSerializer(address_table)

        data = {
            "customer_details": customer_details_serializer.data,
            "address": address_serializer.data,
            "vehicle_details": vehicle_details_serializer.data,
            "ticket_details": ticket_serializer.data,
        }
        response = {'status': 'OK', 'status_code': 200, 'data': data}
        return Response(response)
        
    except Exception as ex:
        error = f'Error in retrieving ticket summary with error: {ex}'
        err_response = {'status': 'Bad Request', 'status_code': 400, 'message': error, 'error_status': True}
        return Response(err_response)


@api_view(['GET', 'POST'])
@permission_classes([IsAuthenticated])
def ticket_list(request):
    try:
        if request.method == 'GET':
            set_delayed_cron()
            logged_in_user = request.user
            user = User.objects.filter(id=logged_in_user.id).get()
            if user.user_type=="dealer":
                all_tickets = Ticket.objects.filter(created_by_id=logged_in_user.id, archive=False)
            elif user.user_type == 'cs_area':
                all_tickets = Ticket.objects.filter(assigned_to_csat=None, user_grp=user.user_type, archive=False) | Ticket.objects.filter(assigned_to_csat=logged_in_user.id, archive=False) | Ticket.objects.filter(assigned_to_csat=None, csat_delayed = True, archive=False) 
            elif user.user_type == 'cs_ho':
                all_tickets = Ticket.objects.filter(assigned_to_csho=None, user_grp=user.user_type, archive=False) | Ticket.objects.filter(assigned_to_csho=logged_in_user.id, archive=False) | Ticket.objects.filter(assigned_to_csho=None, csho_delayed = True, archive=False) 
            else:
                all_tickets = Ticket.objects.filter(assigned_to_rf=None, user_grp=user.user_type, archive=False) | Ticket.objects.filter(assigned_to_rf=logged_in_user.id, archive=False) | Ticket.objects.filter(assigned_to_rf=None, rf_delayed = True, archive=False) 
            ticket_serializer = TicketSerailizer(all_tickets.order_by('-created_on'), many=True)
            if user.user_type != "dealer":
                for ticket in ticket_serializer.data:
                    try:
                        ticket_response = TicketResponse.objects.filter(ticket_id=ticket['id'], user_status=user.user_type).get()
                        ticket['responded_time'] = ticket_response.created_time
                    except Exception as ex:
                        ticket['responded_time'] = "Ticket response does not exist!"

            response = {'status': 'OK', 'status_code': 200, 'data': ticket_serializer.data}
            return Response(response)

        elif request.method == 'POST':
            search_string = request.data['search']
            logged_in_user = request.user
            user = User.objects.filter(id=logged_in_user.id).get()

            if user.user_type=="dealer":
                all_tickets = Ticket.objects.filter(created_by_id=logged_in_user.id)
            elif user.user_type == 'cs_area':
                all_tickets = Ticket.objects.filter(assigned_to_csat=None, user_grp=user.user_type) | Ticket.objects.filter(assigned_to_csat=logged_in_user.id) | Ticket.objects.filter(assigned_to_csat=None, csat_delayed = True) 
            elif user.user_type == 'cs_ho':
                all_tickets = Ticket.objects.filter(assigned_to_csho=None, user_grp=user.user_type) | Ticket.objects.filter(assigned_to_csho=logged_in_user.id) | Ticket.objects.filter(assigned_to_csho=None, csho_delayed = True) 
            else:
                all_tickets = Ticket.objects.filter(assigned_to_rf=None, user_grp=user.user_type) | Ticket.objects.filter(assigned_to_rf=logged_in_user.id) | Ticket.objects.filter(assigned_to_rf=None, rf_delayed = True) 
            queryset = all_tickets.filter(vehicle_details__customer_details__customer_Name__icontains = search_string )
            ticket_serializer = TicketSerailizer(queryset.order_by('-created_on'), many=True)
            if user.user_type != "dealer":
                for ticket in ticket_serializer.data:
                    try:
                        ticket_response = TicketResponse.objects.filter(ticket_id=ticket['id'], user_status=user.user_type).get()
                        ticket['responded_time'] = ticket_response.created_time
                    except Exception as ex:
                        ticket['responded_time'] = "Ticket response does not exist!"

            response = {'status': 'OK', 'status_code': 200, 'data': ticket_serializer.data}
            return Response(response)

    except Exception as ex:
        error = f'Error in retrieving ticket list with error: {ex}'
        err_response = {'status': 'Bad Request', 'status_code': 400, 'message': error, 'error_status': True}
        return Response(err_response)


@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def ticket_delete(request):
    try:
        ticket_id = request.data['ticket_id']
        if Ticket.objects.filter(id=ticket_id).exists():
            queryset =Ticket.objects.filter(id=ticket_id)
            archive = {'archive': request.data['archive']}
            queryset.update(**archive)
            response = {'status': 'OK', 'status_code': 200, 'message': 'Ticket has been deleted.', 'ticket_id': ticket_id}
        else:
            response = {'status': 'No Content', 'status_code': 204, 'message': 'Ticket does not exist.', 'ticket_id': ticket_id}

        return Response(response)

    except Exception as ex:
        error = f'Error in deleting ticket with error: {ex}'
        err_response = {'status': 'Bad Request', 'status_code': 400, 'message': error, 'error_status': True}
        return Response(err_response)


@api_view(['GET', 'POST'])
@permission_classes([IsAuthenticated])
def ticket_response(request):
    try:
        if request.method == 'GET':
            ticket_id = request.GET['ticket_id']
            ticket = Ticket.objects.filter(id=ticket_id).get()
            user_status = ticket.user_grp
            criterion1 = Q(ticket_id=ticket_id)
            ticket_info = TicketResponse.objects.filter(criterion1)
            workflow_info = Workflow.objects.get(ticket=ticket_id)
            
            ticket_info_serializer = TicketResponseSerailizer(ticket_info, many=True)
            serialized_data = ticket_info_serializer.data
            for ticket_info in serialized_data:
                if ticket_info["user_status"] == "cs_area":
                    ticket_info["description"] = workflow_info.description_1
                elif ticket_info["user_status"] == "cs_ho":
                    ticket_info["description"] = workflow_info.description_2
                elif ticket_info["user_status"] == "cs_rf":
                    ticket_info["description"] = workflow_info.description_3

            response = {'status': 'OK', 'status_code': 200, 'data': serialized_data}
            return Response(response)
            
        elif request.method == 'POST':
            ticket_response_data = request.POST
            logged_in_user = request.user
            ticket_id = ticket_response_data["ticket_id"]
            ticket = Ticket.objects.get(id=ticket_id)
            ticket_csat_status = ticket.ticket_csat_status
            ticket_csho_status = ticket.ticket_csho_status
            ticket_rf_status = ticket.ticket_rf_status

            ticket_response_table = TicketResponse()

            ticket_response_table.ticket_id = ticket
            ticket_response_table.user_status = ticket.user_grp
            ticket_response_table.response_type = ticket_response_data["response_type"]
            ticket_response_table.response = ticket_response_data["response"]

            if ticket_response_data["response_type"] == "Offer":
                ticket_response_table.down_payment = ticket_response_data["down_payment"]
                ticket_response_table.int_offered = ticket_response_data.get("int_offered", None)
                ticket_response_table.emi = ticket_response_data.get("emi", None)
                ticket_response_table.tenure = ticket_response_data.get("tenure", None)


            ticket_response_table.save()
            ticket_dealer_status = 'action_pending'
            if ticket.user_grp == 'cs_area':
                Ticket.objects.filter(id=ticket_id).update(ticket_csat_status = 'in_progress',ticket_dealer_status=ticket_dealer_status)
                Workflow.objects.filter(ticket=ticket_id).update(p1e2=timezone.now(), p1s3=timezone.now(), p1t2=2)
            elif ticket.user_grp == 'cs_ho':
                Ticket.objects.filter(id=ticket_id).update(ticket_csho_status = 'in_progress', ticket_dealer_status=ticket_dealer_status)
                Workflow.objects.filter(ticket=ticket_id).update(p2e1=timezone.now(), p2s2=timezone.now(), p2t1=2)
            elif ticket.user_grp == 'cs_rf':
                Ticket.objects.filter(id=ticket_id).update(ticket_rf_status = 'in_progress',ticket_dealer_status=ticket_dealer_status)
                Workflow.objects.filter(ticket=ticket_id).update(p3e1=timezone.now(), p3s2=timezone.now(), p3t1=2)


            if ticket.assigned_to_csat is None:
                Ticket.objects.filter(id=ticket_id).update(assigned_to_csat=logged_in_user)
            elif ticket.assigned_to_csat is not None and ticket.assigned_to_csho is None:
                Ticket.objects.filter(id=ticket_id).update(assigned_to_csho=logged_in_user)
            else:
                Ticket.objects.filter(id=ticket_id).update(assigned_to_rf=logged_in_user)

            response = {'status': 'OK', 'status_code': 200, 'message': 'Ticket Response has been saved.',
                        'ticket_resp_id': ticket_response_table.id}
            return JsonResponse(response, safe=False)
    except Exception as ex:
        error = f'Error in Ticket Response with error: {ex}'
        return JsonResponse({'status': 'Bad Request', 'status_code': 400, 'message': error, 'error_status': True}, safe=False)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def workflow_details(request):

    try:
        if request.method == 'POST':
            ticket = request.data["ticket"]
            user_grp = request.data["user_grp"]
            logged_user = request.user
            
            user = CustomUser.objects.get(email=logged_user)

            #If the current user is allowed to view the ticket.
            if user.user_type == user_grp == "dealer":
                ticket_selected = Ticket.objects.filter(id=ticket, archive=False, created_by=logged_user.id)
            elif user.user_type == user_grp == "cs_area":
                ticket_selected = Ticket.objects.filter(id=ticket, archive=False, assigned_to_csat=logged_user.id) | Ticket.objects.filter(id=ticket, archive=False, user_grp=user_grp, assigned_to_csat=None)
            elif user.user_type == user_grp == "cs_ho":
                ticket_selected = Ticket.objects.filter(id=ticket, archive=False, assigned_to_csho=logged_user.id) | Ticket.objects.filter(id=ticket, archive=False, user_grp=user_grp, assigned_to_csho=None)                
            elif user.user_type == user_grp == "cs_rf":
                ticket_selected = Ticket.objects.filter(id=ticket, archive=False, assigned_to_rf=logged_user.id) | Ticket.objects.filter(id=ticket, archive=False, user_grp=user_grp, assigned_to_rf=None)
            else:
                response = {'status': 'Unauthorized', 'status_code': 401, 'message': "User is not authorized to view details of this ticket!", 'error_status': True }
                return Response(response)            
            #If ticket exists with access get workflow associated with it.
            if ticket_selected:
                #Create JSON object to be passed as response.
                queryset = Workflow.objects.filter(ticket=ticket)
                serializer = WorkflowSerailizer(queryset, many=True)
                obj = workflow_details_json()
                workflow_data = serializer.data[0]
                selected_ticket = ticket_selected[0]                
                # if delayed on a paritcular level update status accordingly
                if selected_ticket.csat_delayed == True:
                    workflow_data["p1t2"] = 3
                
                if selected_ticket.csho_delayed == True:
                    workflow_data["p2t1"] = 3
                
                if selected_ticket.rf_delayed == True:
                    workflow_data["p3t1"] = 3
                # -----------------------                
                #check if any response on current level if not set action boolean true for non dealer user
                if user_grp=="cs_area" and selected_ticket.csat_delayed==False and workflow_data["p1e2"] == None:
                    workflow_data["action_1"] = True
                else:
                    workflow_data["action_1"] = False
                if user_grp=="cs_ho" and selected_ticket.csho_delayed==False and workflow_data["p2e1"] == None:
                    workflow_data["action_2"] = True
                else:
                    workflow_data["action_2"] = False
                if user_grp=="cs_rf" and selected_ticket.rf_delayed==False and workflow_data["p3e1"] == None:
                    workflow_data["action_3"] = True
                else:
                    workflow_data["action_3"] = False
                # -----------------------                
                #check if any response on current level if not set action boolean true for dealer user
                if user_grp=="dealer" and selected_ticket.user_grp=="cs_area" and selected_ticket.csat_delayed==False and workflow_data["p1e2"] != None and workflow_data["p1e3"] == None:
                    workflow_data["dealer_action_1"] = True
                else:
                    workflow_data["dealer_action_1"] = False
                if user_grp=="dealer" and selected_ticket.user_grp=="cs_ho" and selected_ticket.csho_delayed==False and workflow_data["p2e1"] != None and workflow_data["p2e2"] == None:
                    workflow_data["dealer_action_2"] = True
                else:
                    workflow_data["dealer_action_2"] = False
                if user_grp=="dealer" and selected_ticket.user_grp=="cs_rf" and selected_ticket.rf_delayed==False and workflow_data["p3e1"] != None and workflow_data["p3e2"] == None:
                    workflow_data["dealer_action_3"] = True
                else:
                    workflow_data["dealer_action_3"] = False
                # -----------------------                
                json_data_to_send = obj.create_json(dict(workflow_data))               
                response = {'status': 'OK', 'status_code': 200, 'data': json_data_to_send}
                return Response(response)
    except Exception as ex:
        error = f'Error: {ex}'
        err_response = {'status': 'Bad Request', 'status_code': 400, 'message': error, 'error_status': True}
        return Response(err_response)


@api_view(['GET', 'POST'])
@permission_classes([IsAuthenticated])
def ticket_response_details(request):

    try:
        if request.method == 'POST':
            ticket = request.data["ticket"]
            user_grp = request.data["user_grp"]
            logged_user = request.user
            
            user = CustomUser.objects.get(email=logged_user)

            #If the current user is allowed to view the ticket.
            if user.user_type == user_grp == "dealer":
                ticket_selected = Ticket.objects.filter(id=ticket, archive=False, created_by=logged_user.id)
            elif user.user_type == user_grp == "cs_area":
                ticket_selected = Ticket.objects.filter(id=ticket, archive=False, assigned_to_csat=logged_user.id) | Ticket.objects.filter(id=ticket, archive=False, user_grp=user_grp, assigned_to_csat=None)
            elif user.user_type == user_grp == "cs_ho":
                ticket_selected = Ticket.objects.filter(id=ticket, archive=False, assigned_to_csho=logged_user.id) | Ticket.objects.filter(id=ticket, archive=False, user_grp=user_grp, assigned_to_csho=None)                
            elif user.user_type == user_grp == "cs_rf":
                ticket_selected = Ticket.objects.filter(id=ticket, archive=False, assigned_to_rf=logged_user.id) | Ticket.objects.filter(id=ticket, archive=False, user_grp=user_grp, assigned_to_rf=None)
            else:
                response = {'status': 'Unauthorized', 'status_code': 401, 'message': "User is not authorized to view details of this ticket!", 'error_status': True }
                return Response(response)            
            #If ticket exists with access get workflow associated with it.
            if ticket_selected.exists():
                #Create JSON object to be passed as response.
                queryset = TicketResponse.objects.filter(ticket_id=ticket)
                serializer = TicketResponseSerailizer(queryset, many=True)
                queryset_2 = Workflow.objects.filter(ticket=ticket)
                serializer_2 = WorkflowSerailizer(queryset_2, many=True)
                workflow_data = dict(serializer_2.data[0])
                obj = ticket_response_details_json()
                res_data = {"id": "", "user_status": "", "response_type": "", "down_payment": "", "int_offered": "", "emi": "", "tenure": "", "response": "", "created_time": "", "ticket_id": "", "dealer_response": "", "dealer_response_type": "", "action": "", "d_r_created_time": ""}
                response_data = [{"cs_area": res_data}, {"cs_ho": res_data}, {"cs_rf": res_data}]
                if serializer.data:
                    for data_dict in serializer.data:
                        res_data = dict(data_dict)
                        if res_data["user_status"] == "cs_area":
                            res_data["dealer_response"] = workflow_data["description_1"]
                            res_data['d_r_created_time'] = workflow_data["p1e3"]
                            if workflow_data["description_1"] and len(workflow_data["description_1"]) > 0:
                                res_data["dealer_response_type"] = "Accepted" if workflow_data["description_1"] == "Accepted" else "Rejected"
                                res_data["action"] = "false"
                            else:
                                res_data["dealer_response_type"] = ""
                                res_data["action"] = "true"
                            response_data[0] = {"cs_area": res_data}  
                        elif res_data["user_status"] == "cs_ho":
                            res_data["dealer_response"] = workflow_data["description_2"]
                            res_data['d_r_created_time'] = workflow_data["p2e2"]
                            if workflow_data["description_2"] and len(workflow_data["description_2"]) > 0:
                                res_data["dealer_response_type"] = "Accepted" if workflow_data["description_2"] == "Accepted" else "Rejected"
                                res_data["action"] = "false"
                            else:
                                res_data["dealer_response_type"] = ""
                                res_data["action"] = "true"
                            response_data[1] = {"cs_ho": res_data}
                        elif res_data["user_status"] == "cs_rf":
                            res_data["dealer_response"] = workflow_data["description_3"]
                            res_data['d_r_created_time'] = workflow_data["p3e2"]
                            if workflow_data["description_3"] and len(workflow_data["description_3"]) > 0:
                                res_data["dealer_response_type"] = "Accepted" if workflow_data["description_3"] == "Accepted" else "Rejected"
                                res_data["action"] = "false"
                            else:
                                res_data["dealer_response_type"] = ""
                                res_data["action"] = "true"  
                            response_data[2] = {"cs_rf": res_data}
                json_data_to_send = obj.create_json(response_data)               
                response = {'status': 'OK', 'status_code': 200, 'data': json_data_to_send}
                return Response(response)
            else:
                response = {'status': 'Not Found', 'status_code': 404, 'message': "Entry not found!", 'error_status': True}
                return Response(response)
    except Exception as ex:
        error = f'Error: {ex}'
        err_response = {'status': 'Bad Request', 'status_code': 400, 'message': error, 'error_status': True}
        return Response(err_response)





# @api_view(['POST'])
# @permission_classes([IsAuthenticated])
# def offer_approval(request):
#     try:
#         if request.method == 'POST':
#             ticket = request.data["ticket"]
#             level = request.data["level"]
#             approval_status = request.data["approval_status"]
#             message = request.data["message"]
#             logged_user = request.user
            
#             user = CustomUser.objects.get(email=logged_user)

#             # status change for next level
#             if approval_status == "rejected":
#                 next_level_status = "action_pending"
#             else:
#                 next_level_status = "---"

#             if user.user_type == "dealer" and level == "cs_area":
#                 Ticket.objects.filter(id=ticket, user_grp=level, created_by=logged_user.id).update(ticket_dealer_status="in_progress", ticket_csho_status=next_level_status)
#                 Workflow.objects.filter(ticket=ticket).update(process_1=2, description_1=message, p1e3=timezone.now(), p2t1=2, process_2=1, p2s1=timezone.now())
#             response = {'status': 'OK', 'status_code': 200, 'data': "sccuess"}  
#             return Response(response)
#     except Exception as ex:
#         error = f'Error: {ex}'
#         err_response = {'status': 'Bad Request', 'status_code': 400, 'message': error, 'error_status': True}
#         return Response(err_response)

class Workflow_API(APIView):

    def get(self, request, ticket):

        try:
            queryset = Workflow.objects.filter(ticket=ticket)
            serializer = WorkflowSerailizer(queryset, many=True)
            obj = workflow_json()
            json_data_to_send = obj.create_json(dict(serializer.data[0]))

            response = {'status': 'OK', 'status_code': 200, 'data': json_data_to_send}           
            return Response(response)
            #return JsonResponse(json_data_to_send)

        except Exception as ex:
            error = f'Error in retrieving ticket list with error: {ex}, Ticket ID not exist'
            err_response = {'status': 'Bad Request', 'status_code': 400, 'message': error, 'error_status': True}
            return Response(err_response)
            

    def post(self, request, format=None):
        data = request.data.copy()
        queryset = Workflow.objects.filter(ticket = request.data['ticket'])

        if queryset.exists():
            return Response("Ticket ID already exists. Use GET method to fetch details")

        else:
            ticket_table = Ticket.objects.get(id=request.data['ticket'])
            vehicle_details_table = ticket_table.vehicle_details
            customer_details_table = vehicle_details_table.customer_details
 
            data["p1s1"] = customer_details_table.created_on
            data["p1e1"] = ticket_table.created_on
            data["p1s2"] = data["p1e1"]

            serializer = WorkflowSerailizer(data=data)

            if serializer.is_valid():
                serializer.save()

                response = {'status': 'OK', 'status_code': 200, 'data': serializer.data}           
                return Response(response)

            else:
                response = {'status': 'Bad Request', 'status_code': 400, 'data': serializer.errors}           
                return Response(response)
                

    def put(self, request, format=None):

        data = request.data
        ticket_id = data["ticket"]
        message = "Status already updated."

        serializer = WorkflowSerailizer(data=data)  #Data validation 
        if serializer.is_valid():                   #Data validation
            dict_data = serializer.data
            json_data = dict_data
            if json_data['p1t2'] == 2 and json_data['p1t3'] == 2:
                json_data["p1e2"] = datetime.datetime.now()
                json_data["p1t3"] = 1
                json_data["p1s3"] = datetime.datetime.now()                
                Workflow.objects.filter(ticket=ticket_id).update(p1e2=json_data["p1e2"], p1t3=json_data["p1t3"], p1s3=json_data["p1s3"])
                message = "Status Updated"

            elif json_data['p1t3'] == 2 and json_data['description_1'] == "Accepted":
                json_data["p1e3"] = datetime.datetime.now()
                json_data["p1t3"] = 2
                json_data["process_1"] = 2                
                Ticket.objects.filter(id=ticket_id).update(ticket_dealer_status='resolved', ticket_csat_status='completed')
                Workflow.objects.filter(ticket=ticket_id).update(p1e3=json_data["p1e3"], p1t3=json_data["p1t3"], description_1=json_data['description_1'], process_1=json_data["process_1"])
                message = "Status Updated"

            elif json_data['p1t3'] == 2 and json_data['description_1'] != "Accepted":                
                json_data["p1e3"] = datetime.datetime.now()
                json_data["process_1"] = 2
                json_data["process_2"] = 1
                json_data["p2t1"] = 1
                json_data["p2s1"] = datetime.datetime.now()                
                Ticket.objects.filter(id=ticket_id).update(user_grp='cs_ho', ticket_dealer_status='in_progress', ticket_csat_status='completed', ticket_csho_status='action_pending', csho_assign_time = timezone.now())
                Workflow.objects.filter(ticket=ticket_id).update(p1e3=json_data["p1e3"], p1t3=json_data["p1t3"], process_2=json_data["process_2"], process_1=json_data["process_1"], p2t1=json_data["p2t1"], p2s1=json_data["p2s1"], description_1=json_data['description_1'])
                message = "Status Updated"

            elif json_data['p2t1'] == 2:
                json_data["p2e1"] = datetime.datetime.now()
                json_data["p2t2"] = 1
                json_data["p2s2"] = datetime.datetime.now()                
                Workflow.objects.filter(ticket=ticket_id).update(p2t1=json_data["p2t1"], p2e1=json_data["p2e1"], p2t2=json_data["p2t2"],p2s2=json_data["p2s2"])
                message = "Status Updated"

            elif json_data['p2t2'] == 2 and json_data['description_2'] == "Accepted":
                json_data["p2e2"] = datetime.datetime.now()
                json_data["process_2"] = 2                
                Ticket.objects.filter(id=ticket_id).update(ticket_dealer_status='resolved', ticket_csho_status='completed')
                Workflow.objects.filter(ticket=ticket_id).update(p2t2=json_data["p2t2"], p2e2=json_data["p2e2"], description_2=json_data['description_2'], process_2=json_data["process_2"])
                message = "Status Updated"

            elif json_data['p2t2'] == 2 and json_data['description_2'] != "Accepted":
                json_data["p2e2"] = datetime.datetime.now()
                json_data["process_2"] = 2
                json_data["process_3"] = 1
                json_data["p3t1"] = 1
                json_data["p3s1"] = datetime.datetime.now()                
                Ticket.objects.filter(id=ticket_id).update(user_grp='cs_rf', ticket_dealer_status='in_progress', ticket_csho_status='completed', ticket_rf_status='action_pending', rf_assign_time = timezone.now())
                Workflow.objects.filter(ticket=ticket_id).update(p2t2=json_data["p2t2"], p2e2=json_data["p2e2"], description_2=json_data['description_2'], process_3=json_data["process_3"], p3t1=json_data["p3t1"], p3s1=json_data["p3s1"])
                message = "Status Updated"

            elif json_data['p3t1'] == 2:
                json_data["p3e1"] = datetime.datetime.now()
                json_data["p3t2"] = 1
                json_data["p3s2"] = datetime.datetime.now()                
                Workflow.objects.filter(ticket=ticket_id).update(p3t1=json_data["p3t1"], p3e1=json_data["p3e1"], p3t2=json_data["p3t2"], p3s2=json_data["p3s2"])
                message = "Status Updated"

            elif json_data['p3t2'] == 2 and json_data['description_3'] == "Accepted":
                json_data["p3e2"] = datetime.datetime.now()
                json_data["process_3"] = 2                
                Ticket.objects.filter(id=ticket_id).update(ticket_dealer_status='resolved', ticket_rf_status='completed')
                Workflow.objects.filter(ticket=ticket_id).update(p3t2=json_data["p3t2"], p3e2=json_data["p3e2"], description_3=json_data['description_3'], process_3=json_data["process_3"])
                message = "Status Updated"

            elif json_data['p3t2'] == 2 and json_data['description_3'] != "Accepted":
                json_data["p3e2"] = datetime.datetime.now()
                json_data["process_3"] = 2                
                Ticket.objects.filter(id=ticket_id).update(ticket_dealer_status='unresolved', ticket_rf_status='completed')
                Workflow.objects.filter(ticket=ticket_id).update(p3t2=json_data["p3t2"], p3e2=json_data["p3e2"], description_3=json_data['description_3'], process_3=json_data["process_3"])
                message = "Status Updated"
            queryset = Workflow.objects.filter(ticket=ticket_id)
            serializer = WorkflowSerailizer(queryset, many=True)
            response_data = dict(serializer.data[0])
            response = {'status': 'OK', 'status_code': 200, 'data': response_data}
        else:
            response = {'status': 'Bad Request', 'status_code': 400, 'message': serializer.errors}           
            
        #response = {'status': 'OK', 'status_code': 200, 'message': "Status Updated"}
        return Response(response)
