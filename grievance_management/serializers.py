from django.contrib.auth.models import User
from rest_framework import serializers
from grievance_management import models as tables
from django.contrib.postgres import fields
# from .models import *


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = tables.Address
        fields = '__all__'


class CustomerDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = tables.CustomerDetails
        # fields = '__all__'
        exclude = ('address',)


class VehicleDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = tables.VehicleDetails
        fields = '__all__'


class TicketSerailizer(serializers.ModelSerializer):
    customer_name = serializers.CharField(source='vehicle_details.customer_details.customer_Name')
    class Meta:
        model = tables.Ticket
        fields = '__all__'

class TicketResponseSerailizer(serializers.ModelSerializer):
    class Meta:
        model = tables.TicketResponse
        fields = '__all__'


class WorkflowSerailizer(serializers.ModelSerializer):
    class Meta:
        model = tables.Workflow
        fields = '__all__'


