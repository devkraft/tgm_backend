import datetime

class workflow_json:
    def __init__(self):
        pass
    def create_json(self, fields):
        

        json_data = {
            "process": 
            [

                { "process_1": 
                [
                    {
                        "task_1":
                        {
                            "name":"Ticket created",
                            "status": fields["p1t1"],
                            "responsible": "Dealer",
                            "start_time":fields["p1s1"],
                            "end_time":fields["p1e1"]

                        }

                    },
                    {
                        "task_2":
                        {
                            "name":"Ticket under review with CS Area team",
                            "status": fields["p1t2"],
                            "responsible": "CS Area Team",
                            "start_time":fields["p1s2"],
                            "end_time":fields["p1e2"]

                        }

                    },
                    {
                        "task_3":
                        {
                            "name":"Response on Resolution/ Offer",
                            "status": fields["p1t3"],
                            "responsible": "Dealer",
                            "start_time":fields["p1s3"],
                            "end_time":fields["p1e3"],
                            "description": fields["description_1"]

                        }

                    }



                ]

                },

                { "process_2": 
                [
                    {
                        "task_1":
                        {
                            "name":"Ticket under review with CS Headoffice",
                            "status": fields["p2t1"],
                            "responsible": "CS Headoffice",
                            "start_time":fields["p2s1"],
                            "end_time":fields["p2e1"]

                        }

                    },
                    {
                        "task_2":
                        {
                            "name":"Response on Resolution/ Offer",
                            "status": fields["p2t2"],
                            "responsible": "Dealer",
                            "start_time":fields["p2s2"],
                            "end_time":fields["p2e2"],
                            "description": fields["description_2"]

                        }

                        }
                ]
                },

                { "process_3": 
                [
                    {
                        "task_1":
                        {
                            "name":"Ticket under review with Retail Finance Headoffice",
                            "status": fields["p3t1"],
                            "responsible": "Retail Finance Headoffice",
                            "start_time":fields["p3s1"],
                            "end_time":fields["p3e1"]

                        }

                    },
                    {
                        "task_2":
                        {
                            "name":"Response on Resolution/ Offer",
                            "status": fields["p3t2"],
                            "responsible": "Dealer",
                            "start_time":fields["p3s2"],
                            "end_time":fields["p3e2"],
                            "description": fields["description_3"]

                        }

                    }
                ]
                }

            ]

        }
                                         

        return json_data 

class workflow_details_json:
    def __init__(self):
        pass
    
    def create_json(self, fields):
        json_data = {
            "process": 
            [
                { "dealer": 
                    {
                        "ticket_created":{
                        "name":"Ticket created",
                        "status": "Completed" if fields["p1t1"]==2 else ("In Progress" if fields["p1t1"]==1 else "-"),
                        "responsible": "Dealer",
                        "start_time":fields["p1s1"],
                        "end_time":fields["p1e1"],
                        "action": False
                        }
                    }
                },
                { "cs_area":
                    {
                        "response_from_cs_area":{
                            "name":"Ticket under review with CS Area team",
                            "status": "Completed" if fields["p1t2"]==2 else ("In Progress" if fields["p1t2"]==1 else ("Delayed" if fields["p1t2"]>=3 else "-")),
                            "responsible": "CS Area Team",
                            "start_time":fields["p1s2"],
                            "end_time":fields["p1e2"],
                            "action": fields["action_1"]

                        },
                        "dealer_response":{
                            "name":"Response on Resolution/ Offer",
                            "status": "Accepted" if fields["p1t3"]==2 and fields['description_1'] == "Accepted" else ("Rejected" if fields["p1t3"]==2 and fields['description_1'] != "Accepted" else ("In Progress"if fields["p1t3"]==1 else "-")),
                            "responsible": "Dealer",
                            "start_time":fields["p1s3"],
                            "end_time":fields["p1e3"],
                            "description": fields["description_1"],
                            "action": fields["dealer_action_1"]
                        }
                    }
                },
                { "cs_ho": 
                    {
                        "response_from_cs_ho":{
                            "name":"Ticket under review with CS Head Office",
                            "status": "Completed" if fields["p2t1"]==2 else ("In Progress" if fields["p2t1"]==1 else ("Delayed" if fields["p2t1"]==3 else "-")),
                            "responsible": "Head Office",
                            "start_time":fields["p2s1"],
                            "end_time":fields["p2e1"],
                            "action": fields["action_2"]
                        },
                        "dealer_response":{
                            "name":"Response on Resolution/ Offer",
                            "status": "Accepted" if fields["p2t2"]==2 and fields['description_2'] == "Accepted" else ("Rejected" if fields["p2t2"]==2 and fields['description_2'] != "Accepted" else ("In Progress" if fields["p2t2"]==1 else "-")),
                            "responsible": "Dealer",
                            "start_time":fields["p2s2"],
                            "end_time":fields["p2e2"],
                            "description": fields["description_2"],
                            "action": fields["dealer_action_2"]
                        }
                    }
                },
                { "cs_rf": 
                    {
                        "response_from_retail_finance":{
                            "name":"Ticket under review with Retail Head Office",
                            "status": "Completed" if fields["p3t1"]==2 else ("In Progress" if fields["p3t1"]==1 else ("Delayed" if fields["p3t1"]==3 else "-")),
                            "responsible": "Retail Head Office",
                            "start_time":fields["p3s1"],
                            "end_time":fields["p3e1"],
                            "action": fields["action_3"]
                        },
                        "dealer_response":{
                            "name":"Response on Resolution/ Offer",
                            "status": "Accepted" if fields["p3t2"]==2 and fields['description_3'] == "Accepted" else ("Rejected" if fields["p3t2"]==2 and fields['description_3'] != "Accepted" else ("In Progress" if fields["p3t2"]==1 else "-")),
                            "responsible": "Dealer",
                            "start_time":fields["p3s2"],
                            "end_time":fields["p3e2"],
                            "description": fields["description_3"],
                            "action": fields["dealer_action_3"]
                        }
                    }   
                }
            ]
        }
        return json_data
