from django.contrib import admin

from grievance_management.views import ticket_response
from .models import *
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from .forms import UserAdminCreationForm
from django.contrib.auth import get_user_model
User = get_user_model()

admin.site.register(Address)
admin.site.register(CustomerDetails)
admin.site.register(VehicleDetails)
admin.site.register(Ticket)
admin.site.register(Workflow)
admin.site.register(TicketResponse)

class UserAdmin(BaseUserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password', 'user_type', 'last_login')}),
        ('Permissions', {'fields': (
            'is_active', 
            'is_staff', 
            'is_superuser',
            'groups', 
            'user_permissions',
        )}),
    )
    add_fieldsets = (
        (
            None,
            {
                'classes': ('wide',),
                'fields': ('email', 'user_type', 'password1', 'password2')
            }
        ),
    )

    list_display = ('email', 'user_type','username','last_login',)
    list_filter = ('user_type',)
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()


admin.site.register(User, UserAdmin)