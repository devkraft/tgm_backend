import re
import json
from rest_framework.response import Response
from django.db.models.query import QuerySet
from .serializers import * 
from grievance_management.models import *
from datetime import datetime, timedelta
import logging
from django.http import HttpResponse
import os
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent

log_file_path = str(BASE_DIR.parent) + "/logs/cron.log"
logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'console': {
            'format': '%(name)-12s %(levelname)-8s %(message)s'
        },
        'file': {
            'format': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'console'
        },
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'formatter': 'file',
            'filename': log_file_path
        }
    },
    'loggers': {
        '': {
            'level': 'DEBUG',
            'handlers': ['console', 'file']
        },
        'django.request': {
            'level': 'DEBUG',
            'handlers': ['console', 'file']
        }
    }
})

logger = logging.getLogger(__name__)

def set_delayed_cron():
    try:
        time_threshold = datetime.now() - timedelta(hours=48)
        if Ticket.objects.filter(csat_assign_time__lt=time_threshold, ticket_csat_status = "action_pending").exists():
            Ticket.objects.filter(csat_assign_time__lt=time_threshold, ticket_csat_status = "action_pending").update(csat_delayed=True, user_grp='cs_ho', ticket_csat_status = "delayed", ticket_csho_status="action_pending", csho_assign_time=timezone.now())
            Workflow.objects.filter(p1s2__lt=time_threshold, p1t2=1).update(p2t1=1, p2s1=timezone.now())
        elif Ticket.objects.filter(csho_assign_time__lt=time_threshold, ticket_csho_status = "action_pending").exists():
            Ticket.objects.filter(csho_assign_time__lt=time_threshold, ticket_csho_status = "action_pending").update(csho_delayed=True, user_grp='cs_rf', ticket_csho_status = "delayed", ticket_rf_status="action_pending", rf_assign_time=timezone.now())
            Workflow.objects.filter(p2s1__lt=time_threshold, p2t1=1).update(p3t1=1, p3s1=timezone.now())
        elif Ticket.objects.filter(rf_assign_time__lt=time_threshold, ticket_rf_status = "action_pending").exists():
            Ticket.objects.filter(rf_assign_time__lt=time_threshold, ticket_rf_status = "action_pending").update(csrf_delayed=True, user_grp='---', ticket_csrf_status = "delayed", ticket_dealer_status="unresolved",)
        
        info_msg = "Ran cron job successfully at " + str(timezone.now()) 
        logger.info(info_msg)
        success_response = {'message':info_msg, 'success_status': True}
        return Response(success_response)

    except Exception as ex:
        error = f'Error in running cron job: {ex}'
        err_response = {'message': error, 'error_status': True}
        err_msg = "Cron job failed to run at " + str(timezone.now()) 
        logger.error(error)
        return Response(err_response)
