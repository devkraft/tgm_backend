from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, User
)
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils import timezone
# Cutom User to define user_type and other future operations

class UserManager(BaseUserManager):


    use_in_migrations = True

    def _create_user(self, email, password, user_type, is_staff, is_superuser, **extra_fields):
        if not email:
            raise ValueError('Users must have an email address')
        now = timezone.now()
        email = self.normalize_email(email)
        user = self.model(
            email=email,
            user_type=user_type,
            is_staff=is_staff, 
            is_active=True,
            last_login=now,
            date_joined=now, 
            is_superuser=is_superuser, 
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password, **extra_fields):
        return self._create_user(email, password, 'dealer', False, False, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        user=self._create_user(email, password, '---', True, True, **extra_fields)
        return user



class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    username =  models.CharField(max_length=255, null=True)
    user_type_choices = (
        ('---', 'No User Type'),
        ('dealer', 'Dealer'),
        ('cs_area', 'CS Area'),
        ('cs_ho', 'Home Office'),
        ('cs_rf', 'Retail Head Office'))
    user_type = models.CharField(max_length=7, choices=user_type_choices, default='dealer')
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    last_login  = models.DateTimeField(null=True, blank=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    first_name = models.CharField(max_length=50, null=True)
    last_name = models.CharField(max_length=50, null=True)

    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = [] # Email & Password are required by default.
    objects = UserManager()

    class Meta:
        db_table = 'custom_user'
    

    def __str__(self):
        return self.email

    def get_absolute_url(self):
        return "/users/%i/" % (self.pk)
        
from django.contrib.auth import get_user_model
User = get_user_model()

class Address(models.Model):
    address = models.CharField(max_length=300, blank=True, null=True)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    pin = models.IntegerField()

    class Meta:
        db_table = 'address'


class CustomerDetails(models.Model):
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    customer_Name = models.CharField(max_length=230)
    dob = models.DateField()
    marital_choices=[('married', "Married"), ("unmarried", "Unmarried")]
    marital_status = models.CharField(max_length=12, choices=marital_choices)
    income_coices =[('1-3','One-Three'), ('3-6', 'Three-Six'), ( '6-10' ,'Six-Ten'), ( '10-15' ,'Ten-Fifteen'), ( '>15' ,'Greater-than-15') ]
    income = models.CharField(max_length=50, choices=income_coices, default= '1-3')
    income_type_coices =[('salaried','Salaried'), ('self_employed', 'Self Employed'), ( 'not_provided' ,'Not Provided') ]
    income_type = models.CharField(max_length=13, choices=income_type_coices, default= 'not_provided')
    mobile_number = models.CharField(max_length=50)
    pan_no = models.CharField(max_length=10)
    email = models.EmailField()
    address = models.OneToOneField(Address, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True, null=True)

    # kyc = [('dl_id', 'Driving License ID'), ('Aadhar', 'Aadhar'), ('Voter ID', 'Voter ID'),
    #        ('Passport', 'Passport')]
    # kyc_type = models.CharField(max_length=100, choices=kyc)
    # kyc_number = models.CharField(max_length=20)

    aadhar = models.BooleanField(default=False)
    passport = models.BooleanField(default=False)
    driving_license = models.BooleanField(default=False)
    voter_id = models.BooleanField(default=False)
    salary_slip = models.BooleanField(default=False)
    bank_statement = models.BooleanField(default=False)

    class Meta:
        db_table = 'customer_details'


class VehicleDetails(models.Model):
    customer_details = models.OneToOneField(CustomerDetails, on_delete=models.CASCADE)
    model_name = models.CharField(max_length=200, default='NA')
    price = models.IntegerField()
    variant = models.CharField(max_length=230, default='NA')
    down_payment = models.FloatField(null = True, blank = True)
    rate_of_interest = models.FloatField(null = True, blank = True)
    requested_emi = models.FloatField(null = True, blank = True)
    tenure = models.IntegerField(null = True, blank = True)  # in months
    comment = models.TextField(max_length= 300)
    issue_type_choices = [('payment_related', 'Payment related'), ('document_related', 'Document related'), ('other', 'Other')]
    issue_type = models.CharField(max_length=50, choices=issue_type_choices, default = 'other')

    class Meta:
        db_table = 'vehicle_details'



class Ticket(models.Model):
    category = models.IntegerField()
    status_options = [('in_progress', 'In Progress'), ('action_pending', 'Action Pending'), ('resolved', 'Resolved'), ('completed', 'Completed'),
                      ('unresolved', 'Unresolved')]
    non_dealer_status_options = [('in_progress', 'In Progress'), ('action_pending', 'Action Pending'), ('completed', 'Completed'), ('delayed', 'Delayed')]
    ticket_dealer_status = models.CharField(max_length=20, choices=status_options, default='in_progress')
    ticket_csat_status = models.CharField(max_length=100, choices=non_dealer_status_options, default='in_progress')
    ticket_csho_status = models.CharField(max_length=100, choices=non_dealer_status_options, default='in_progress')
    ticket_rf_status = models.CharField(max_length=20, choices=non_dealer_status_options, default='in_progress')
    description = models.TextField(null=True, blank=True)
    remarks = models.CharField(max_length=300, null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    vehicle_details = models.OneToOneField(VehicleDetails, on_delete=models.CASCADE, null=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    assigned_to_csat = models.ForeignKey(User,related_name='assigned_to_csat', on_delete=models.CASCADE, null=True, blank=True)
    assigned_to_csho = models.ForeignKey(User,related_name='assigned_to_csho', on_delete=models.CASCADE, null=True, blank=True)
    assigned_to_rf = models.ForeignKey(User,related_name='assigned_to_rf', on_delete=models.CASCADE, null=True, blank=True)
    csat_delayed = models.BooleanField(default=False)
    csho_delayed = models.BooleanField(default=False)
    rf_delayed = models.BooleanField(default=False)
    csat_assign_time = models.DateTimeField(null=True, blank=True)
    csho_assign_time = models.DateTimeField(null=True, blank=True)
    rf_assign_time = models.DateTimeField(null=True, blank=True)
    user_grp_choices = (
        ('---', 'No Group'),
        ('dealer', 'Dealer'),
        ('cs_area', 'CS Area'),
        ('cs_ho', 'Home Office'),
        ('cs_rf', 'Retail Head Office'))
    user_grp = models.CharField(max_length=7, choices=user_grp_choices, default='cs_area')
    archive = models.BooleanField(default=False)
    class Meta:
        db_table = 'ticket'


class TicketResponse(models.Model):
    user_status = models.CharField(max_length=200)
    ticket_id = models.ForeignKey(Ticket, on_delete=models.CASCADE)
    response_type = models.CharField(max_length=100, null=True)
    down_payment = models.FloatField(null=True, blank=True)
    int_offered = models.FloatField(null=True, blank=True)
    emi = models.FloatField(null=True, blank=True)
    tenure = models.IntegerField(null=True, blank=True)
    response = models.TextField(null=True)
    created_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'ticket_response'


class Workflow(models.Model):
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE)

    class Meta:
        db_table = 'tgm_workflow'


process, task, description, start_timestamp, end_timestamp = [], [], [], [], []
no_of_process = 3
no_of_task = 3  # 3 for process_1 and 2 for the next two processes

for i in range(1, no_of_process + 1):
    process.append('process_' + str(i))
    description.append('description_' + str(i))
    if i == 1:
        for j in range(1, no_of_task + 1):
            task.append('p' + str(i) + 't' + str(j))
            start_timestamp.append('p' + str(i) + 's' + str(j))
            end_timestamp.append('p' + str(i) + 'e' + str(j))
            # responsible.append('p'+ str(i)+ 'r'+ str(j))


    else:
        for j in range(1, no_of_task):
            task.append('p' + str(i) + 't' + str(j))
            start_timestamp.append('p' + str(i) + 's' + str(j))
            end_timestamp.append('p' + str(i) + 'e' + str(j))
            # responsible.append('p'+ str(i)+ 'r'+ str(j))

task_choices =[(1,'In Process'), (2, 'Completed')]
process_choices =[(1,'In Process'), (2, 'Completed')]

for ii in range(0, len(process)):
    Workflow.add_to_class(process[ii], models.IntegerField(null=True, blank=True, choices = process_choices))
    Workflow.add_to_class(description[ii], models.TextField(null=True, blank=True))

for jj in range(0, len(task)):
    Workflow.add_to_class(task[jj], models.IntegerField(null=True, blank=True, choices = task_choices))
    # Workflow.add_to_class(responsible[jj], models.TextField(null=True, blank=True))
    Workflow.add_to_class(start_timestamp[jj], models.DateTimeField(null=True, blank=True))
    Workflow.add_to_class(end_timestamp[jj], models.DateTimeField(null=True, blank=True))
