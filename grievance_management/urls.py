from django.urls import path
from .views import *


urlpatterns = [
    path('login', login),
    path('create_ticket', create_ticket),
    path('customer_details', customer_details),
    path('vehicle_details', vehicle_details),
    path('ticket_summary', ticket_summary),
    path('ticket_list', ticket_list),
    path('ticket_delete', ticket_delete),
    path('ticket_response', ticket_response),
    path('workflow_details', workflow_details),
    path('ticket_response_details', ticket_response_details),
    path('workflow/',
         Workflow_API.as_view(),
         name='workflow_all'),
    path('workflow/<int:ticket>',
         Workflow_API.as_view(),
         name='workflow-ticketID')
]
