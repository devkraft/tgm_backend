#from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
User = get_user_model()
from rest_framework import serializers
#from jwt_auth import models as tables
from django.contrib.postgres import fields
from .models import *
import datetime

from rest_framework_simplejwt.serializers import TokenObtainPairSerializer, TokenRefreshSerializer


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)
        refresh = self.get_token(self.user)
        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)

        # Add extra responses here
        data['email'] = self.user.email
        data['token_validity'] = datetime.datetime.now() + datetime.timedelta(hours=1)
        data['refresh_token_validity'] = datetime.datetime.now() + datetime.timedelta(days=180)
        
        return data


class MyTokenRefreshSerializer(TokenRefreshSerializer):

    def validate(self, attrs):

        data = super().validate(attrs)

        # Add extra responses here
        data['token_validity'] = datetime.datetime.now() + datetime.timedelta(hours=1)
        
        # # Update database
        # queryset = Token.objects.filter(refresh= attrs['refresh'])
        # queryset.update(**data)
        
        return data
