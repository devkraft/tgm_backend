import datetime
from django.contrib.auth import get_user_model
User = get_user_model()
#from .models import *
from . import serializers
from .serializers import *
from rest_framework.response import Response
from rest_framework import generics, status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.permissions import AllowAny
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from django.http import  HttpResponseRedirect
from django.shortcuts import redirect
from rest_framework_simplejwt.exceptions import InvalidToken, TokenError
from django.contrib.auth import logout


def logout_view(request):
    logout(request)


class MyObtainTokenPairView(TokenObtainPairView):
    permission_classes = (AllowAny,)
    # def post(self, request, *args, **kwargs):
        
    #     try:
    #         user_db = None
    #         email = request.data['email']
    #         if User.objects.filter(email=email).exists():
    #             user_db = User.objects.get(email=email)

    #         else:
    #             response = {'message': 'Email does not exist', 'error_status': True,
    #                         'error_code': 4}
    #             return Response(response)

    #         # if user_db:
    #         #     if user_db.check_password(request.data['password']):
                    
    #         #         serializer_class = MyTokenObtainPairSerializer.validate(request.data)
                    
    #         #         # Update database
    #         #         if Token.objects.filter(email= email).exists():
    #         #             queryset = Token.objects.filter(email= email)
    #         #             queryset.update(**serializer_class)

    #         #         else:
    #         #             obj = Token(**serializer_class)
    #         #             obj.save()

    #         #         response = serializer_class
    #         #     else:
    #         #         response = {'message': 'wrong password', 'error_status': True, 'error_code': 2}
    #         # else:
    #         #     response = {'message': 'username/email does not exist', 'error_status': True, 'error_code': 1}

    #         return Response( {'message': 'OK'})

    #     except Exception as ex:
    #         response = {'message': 'Error in' + str(ex), 'error_status': True, 'error_code': 3}
    #         return Response(response)

    serializer_class = MyTokenObtainPairSerializer


class MyObtainTokenRefreshView(TokenRefreshView):
    permission_classes = (AllowAny,)
    # def post(self, request, *args, **kwargs):
        
    #     if request.data['refresh']== '':
    #         logout_view(request)
    #         return redirect('http://13.233.210.184')

    #     else:  
    #         serializer = self.get_serializer(data=request.data)

    #         try:
    #             serializer.is_valid(raise_exception=True)
    #         except TokenError as e:
    #             raise InvalidToken(e.args[0])

    #         return Response(serializer.validated_data, status=status.HTTP_200_OK)

    serializer_class = MyTokenRefreshSerializer