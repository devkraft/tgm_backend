from django.urls import path
from .views import *

urlpatterns = [
    path('token', MyObtainTokenPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh', MyObtainTokenRefreshView.as_view(), name='token_refresh'),
    #path('token/verify', TokenVerifyView.as_view(), name='token_verify'),

]