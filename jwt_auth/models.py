from django.db import models

# Create your models here.

class Token(models.Model):
    email = models.CharField(max_length=50, blank=True, null=True)
    username = models.CharField(max_length=50, blank=True, null=True)
    refresh = models.CharField(max_length=600, blank=True, null=True)
    access = models.CharField(max_length=600, blank=True, null=True)
    token_validity = models.DateTimeField(null=True)
    refresh_token_validity = models.DateTimeField(null=True)
    class Meta:
        db_table = 'token'
