from django.db.models.fields import NullBooleanField
from django.db.models.query import QuerySet
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import Http404
from django.contrib.postgres import fields
from rest_framework.pagination import PageNumberPagination
import datetime
import json
from grievance_management.models import *
from .serializers import * 
from django.db.models import Q
from django.views.generic.base import View
from notifications.models import *
from django.contrib.auth import login, authenticate
from django.contrib.auth import get_user_model
User = get_user_model()

# Create your views here.

@api_view(['GET', 'POST', 'PUT'])
def user_notifications(request):
    try:
        if request.method == 'GET':
            request_user = User.objects.get(id=request.user.id)
            all_notifications = Notifications.objects.filter(actor=request.user) | Notifications.objects.filter(notification_to=request.user) | Notifications.objects.filter(notification_to_grp=request_user.user_type)
            paginator = PageNumberPagination()
            paginator.page_size = 10

            serializer = NotificationsSerializer(paginator.paginate_queryset(all_notifications.order_by('-created_on'), request), many=True)

            response = paginator.get_paginated_response(serializer.data)

            response.data['unread'] = all_notifications.filter(read = False).count()
            response.data['status'] = 'OK'
            response.data['status_code'] = 200
            response.data.move_to_end('unread', last=False)
            response.data.move_to_end('status', last=False)
            response.data.move_to_end('status_code', last=False)

            return Response(response.data)

        elif request.method == 'PUT':
            notification_ = request.data
            if Notifications.objects.filter(id= notification_['notification_id']).exists():
                queryset = Notifications.objects.filter(id= notification_['notification_id'])

                update_ = {'read': notification_['read'],
                            'updated_on': datetime.datetime.now()
                            }
                queryset.update(**update_)

                if notification_['read'] == True:
                    read_status = 'Notification has been marked as read.'
                else:
                    read_status = 'Notification has been marked as unread.'

                response = {'status': 'OK', 'status_code': 200, 'message': read_status, 'notification_id': notification_['notification_id']}
                return JsonResponse(response, safe=False)

            else:
                response = {'status': 'No Content', 'status_code': 204, 'message': "Notification doesn't exist.", 'error_status': True
                            }
                return JsonResponse(response, safe=False)

        elif request.method == 'POST':
            notification_data = request.POST

            ticket_id = notification_data["ticket"]
            if Ticket.objects.filter(id=ticket_id).exists():
                notification_detail = Notifications()
                if notification_data['action']:
                    notification_detail.action = notification_data['action']

                actor = User.objects.get(id=notification_data['actor'])
                notification_detail.actor = actor

                if notification_data['notification_to'] != 'null':
                    notification_to = User.objects.get(id=notification_data['notification_to'])
                    notification_detail.notification_to = notification_to
                else:
                    notification_to = None

                ticket_id = Ticket.objects.get(id=notification_data["ticket"])
                notification_detail.ticket = ticket_id

                if notification_data['notification_to_grp']:
                    notification_detail.notification_to_grp = notification_data['notification_to_grp']
                    notification_detail.message = notification_data['message']


                team = ""
                if notification_detail.notification_to_grp == "cs_area":
                    team = "CS Area"
                if notification_detail.notification_to_grp == "cs_ho":
                    team = "CS Head Office"
                elif notification_detail.notification_to_grp == "cs_rf":
                    team = "Retail Finance Head Office"
                
                message = ""

                if notification_detail.action == "created" and notification_detail.notification_to == notification_detail.actor:
                    message == "Thank you for reaching out! " + team + " has been notified about the issue and will respond to your ticket #" + str(notification_data["ticket"]) + " within next 48 hours. "
                elif notification_detail.action == "created" and notification_detail.notification_to != notification_detail.actor:
                    message = "Ticket #" + str(notification_data["ticket"]) + " has been raised and assigned to " + team + " team for financing issue."
                elif notification_detail.action == "responded":
                    message = "Ticket #" + str(notification_data["ticket"]) + " responded by " + str(actor.email) + " for financing issue."
                elif notification_detail.action == "approved" and notification_detail.notification_to == notification_detail.actor:
                    message = "Congratulations! You have approved the response offer from " + team + " team for ticket #" + str(notification_data["ticket"]) + "."
                elif notification_detail.action == "approved" and notification_detail.notification_to != notification_detail.actor:
                    message = "Response offer, with respect to ticket " + "#" + str(notification_data["ticket"]) + " has been approved by " + str(actor.email) + " for financing issue."
                elif notification_detail.action == "rejected" and notification_detail.notification_to == notification_detail.actor:
                    message = "You have rejected the response offer from " + team + " team for ticket #" + str(notification_data["ticket"]) + ". TIcket has been forwarded to the next level for further process."
                elif notification_detail.action == "rejected" and notification_detail.notification_to != notification_detail.actor:
                    message = "Response offer, with respect to ticket " + "#" + str(notification_data["ticket"]) + " has been rejected by " + str(actor.email) + " for financing issue."
                else:
                    message = "Ticket  #" + str(notification_data["ticket"]) + " has been assigned to you!"
                
                notification_detail.message = message


                notification_detail.save()

                response = {'status': 'OK', 'status_code': 200, 'message': 'Notification Detail has been saved.',
                            'notification_detail_id': notification_detail.id}
                return JsonResponse(response, safe=False)

            else:
                response = {'status': 'No Content', 'status_code': 204, 'message': "Ticket doesn't exist.", 'error_status': True
                            }
                return JsonResponse(response, safe=False)

    except Exception as ex:
        error = f'Error in Notification Response with error: {ex}'
        return JsonResponse({'status': 'Bad Request', 'status_code': 400, 'message': error, 'error_status': True}, safe=False)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def get_notification_by_date(request):
    try:
        if request.method == 'POST':
            start = datetime.datetime.strptime(request.data['start'], '%Y-%m-%d')
            end = datetime.datetime.strptime(request.data['end'], '%Y-%m-%d')

            request_user = User.objects.get(id=request.user.id)
            all_notifications = Notifications.objects.filter(actor=request.user) | Notifications.objects.filter(notification_to=request.user) | Notifications.objects.filter(notification_to_grp=request_user.user_type)
        
            queryset = all_notifications.filter(created_on__range=[start, end])

            paginator = PageNumberPagination()
            paginator.page_size = 10

            serializer = NotificationsSerializer(paginator.paginate_queryset(queryset.order_by('-created_on'), request), many=True)
            
            response = paginator.get_paginated_response(serializer.data)

            response.data['unread'] = all_notifications.filter(read = False).count()
            response.data['status'] = 'OK'
            response.data['status_code'] = 200
            response.data.move_to_end('unread', last=False)
            response.data.move_to_end('status', last=False)
            response.data.move_to_end('status_code', last=False)

            return Response(response.data)


    except Exception as ex:
        error = f'Error in retrieving notification with error: {ex}'
        return JsonResponse({'status': 'Bad Request', 'status_code': 400, 'message': error, 'error_status': True}, safe=False)
        
class get_notification_by_ID(APIView):

    def get(self, request,id):
        try:
            if request.method == 'GET':
                
                request_user = User.objects.get(id=request.user.id)
                all_notifications = Notifications.objects.filter(ticket__id=id)
                
                queryset = all_notifications.filter(actor=request.user) | all_notifications.filter(notification_to=request.user) | all_notifications.filter(notification_to_grp=request_user.user_type)

                paginator = PageNumberPagination()
                paginator.page_size = 10

                serializer = NotificationsSerializer(paginator.paginate_queryset(queryset.order_by('-created_on'), request), many=True)

                response = paginator.get_paginated_response(serializer.data)
                
                response.data['unread'] = all_notifications.filter(read = False).count()
                response.data['status'] = 'OK'
                response.data['status_code'] = 200
                response.data.move_to_end('unread', last=False)
                response.data.move_to_end('status', last=False)
                response.data.move_to_end('status_code', last=False)

                return Response(response.data)

        except Exception as ex:
            error = f'Error in retrieving notification with error: {ex}'
            return JsonResponse({'status': 'Bad Request', 'status_code': 400, 'message': error, 'error_status': True}, safe=False)


        
