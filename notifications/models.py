from django.contrib.auth.models import PermissionsMixin
from django.db import models
from grievance_management.models import Ticket
from django.contrib.auth import get_user_model
User = get_user_model()
# Create your models here.

class Notifications(models.Model):
    action_choices = (
        ('---', 'No Action'),
        ('created', 'Created'),
        ('responded', 'Responded'),
        ('approved', 'Approved'),
        ('rejected', 'Rejected'))
    action = models.CharField(max_length=9, choices=action_choices, default='----', null=True, blank=True)
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE) 
    actor = models.ForeignKey(User, related_name='notfication_actor', on_delete=models.CASCADE, null=True)
    notification_to = models.ForeignKey(User, related_name='notify_to', on_delete=models.CASCADE, null=True, blank= True)
    notification_grp_choices = (
        ('---', 'No Group'),
        ('dealer', 'Dealer'),
        ('cs_area', 'CS Area'),
        ('cs_ho', 'CS HomeOffice'),
        ('cs_rf', 'Retail Finance'))
    notification_to_grp = models.CharField(max_length=7, choices=notification_grp_choices, default='---', null=True, blank= True)
    message = models.TextField(null=True, blank= True)
    created_on = models.DateTimeField(auto_now_add=True, null=True)
    updated_on = models.DateTimeField(null=True,blank=True)
    read = models.BooleanField(default=False)