from django.urls import path
from .views import *


urlpatterns = [
    path('notifications', user_notifications),
    path('notifications/get_by_date', get_notification_by_date),
    path('notifications/<int:id>', get_notification_by_ID.as_view())
]